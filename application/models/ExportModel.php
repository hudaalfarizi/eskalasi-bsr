<?php
/**
 * 
 */
class ExportModel extends CI_Model
{
	function export_report($tipe,$iddata) {
		$this->db->select('
								t.ticket_code,
								u.username,
								tr.date,
								tr.open,
								tr.approve,
								tr.execute,
								tr.pending,
								tr.finish,
								tr.manhours
							');
		$this->db->from('tracelog_ticket tr');
		$this->db->join('user u','u,user_id=tr.pic','left');
		$this->db->join('ticket t','t.ticket_id=tr.ticket_id','left');
		if($tipe == 1) {
			if($iddata != 0) {
				$this->db->where('tr.ticket_id',$iddata);
			}
		} elseif ($tipe == 2) {
			$this->db->where("tr.pic",$iddata);
		}
		$this->db->order_by('t.ticket_code','desc');
		$this->db->order_by('tr.tracelog_ticket_id','asc');
		return $this->db->get()->result();
	}
}