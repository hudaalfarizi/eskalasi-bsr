<?php

class Permission extends CI_Model
{

    public function cek_permission($module = '')
    {
        $position = $this->information_user()->position_id;
        $user_id  = $this->session->user_id;
        if ($this->session->is_login === TRUE) {
            $sql = "SELECT permission_id from permission INNER JOIN module ON permission.module_id=module.module_id WHERE permission.position_id='" . $position . "' AND module.module_name='" . $module . "' OR
                permission.user_id='" . $user_id . "' AND module.module_name='" . $module . "'
             ";
            $data = $this->db->query($sql);
            return ($data->num_rows() != 0);
        }
    }
    public function information_user()
    {
        $get_information = $this->db->select('
                                            u.user_id,
                                            u.username,
											u.idcard,
                                            pf.name,
                                            pf.phone,
                                            pf.email,
											pf.profile_picture,
                                            p.position_name,
                                            p.position_id,
                                            d.departement_id,
                                            d.departement_name,
                                            d.departement_type,
                                        ')
            ->from('user u')
            ->join('profile pf', 'pf.user_id=u.user_id')
            ->join('position p', 'p.position_id=u.position_id')
            ->join('departement d', 'd.departement_id=p.departement_id')
            ->where('u.user_id', $this->session->user_id)
            ->get()->row();
        return $get_information;
    }
    function bulan($bln)
    {
        switch ($bln) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }
    function date_indo()
    {
        $ubah       = gmdate(date('Y-m-d h:i:s'), time() + 60 * 60 * 8);
        $pecah      = explode("-", $ubah);
        $tanggal    = $pecah[2];
        $tanggali   = explode(" ", $tanggal);
        $tanggal    = $tanggali[0];
        $bulan      = $this->bulan($pecah[1]);
        $tahun      = $pecah[0];
        return $tanggal . ' ' . $bulan . ' ' . $tahun;
    }
}
