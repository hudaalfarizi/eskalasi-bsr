<?php

class Notifikasi extends CI_Model {
    function notif_open(){
        $cek  = $this->db->get_where("ticket",["user_id"=>$this->session->user_id,"status"=>"open"])->num_rows();
        $data = array(
            "response" => "success",
            'csrfHash' => $this->security->get_csrf_hash(),
            "msg"      => $cek,
        );
        return $data;
    }
    function notif_masuk(){
        $sql  = "SELECT * FROM ticket WHERE departement_id_destination='" .$this->session->departement_id . "' AND status='open' ";
        $cek  = $this->db->query($sql);
        $data = array(
            "response" => "success",
            'csrfHash' => $this->security->get_csrf_hash(),
            "msg"      => $cek->num_rows(),
        );
        return $data;
    }
    function notif_proses(){
        $did = $this->session->departement_id;
        if($this->session->departement_type == "maintenance") {
            $sql  = "SELECT * FROM ticket WHERE departement_id_destination='". $did ."' AND teknisi='". $this->session->user_id ."' AND (`status`='diproses' OR `status`='menunggu_approve' OR `status`='pending') OR status='pending' ";
            $cek  = $this->db->query($sql);
        } elseif($this->session->departement_type == "user") {
            $sql  = "SELECT * FROM ticket WHERE departement_id_from='". $did ."' AND teknisi='". $this->session->user_id ."' AND (`status`='diproses' OR `status`='menunggu_approve' OR `status`='pending') ";
            $cek  = $this->db->query($sql);
        } else {
            $sql  = "SELECT * FROM ticket WHERE departement_id_from='". $did ."' AND teknisi='". $this->session->user_id ."' AND AND (`status`='diproses' OR `status`='menunggu_approve' OR `status`='pending') ";
            $cek  = $this->db->query($sql);
        }
        $data = array(
            "response" => "success",
            'csrfHash' => $this->security->get_csrf_hash(),
            "msg"      => $cek->num_rows(),
        );
        return $data;
    }
    function notif_approve(){
        if($this->session->departement_type == "maintenance") {
            $cek  = $this->db->get_where("ticket",["departement_id_destination"=>$this->session->departement_id,"status"=>"menunggu_approve"])->num_rows();
        } elseif($this->session->departement_type == "user") {
            $cek  = $this->db->get_where("ticket",["departement_id_from"=>$this->session->departement_id,"status"=>"menunggu_approve"])->num_rows();
        }
        $data = array(
            "response" => "success",
            'csrfHash' => $this->security->get_csrf_hash(),
            "msg"      => $cek,
        );
        return $data;
    }
    function notif_selesai(){
        if($this->session->departement_type == "maintenance") {
            $cek  = $this->db->get_where("ticket",["departement_id_destination"=>$this->session->departement_id,"status"=>"selesai"])->num_rows();
        } elseif($this->session->departement_type == "user") {
            $cek  = $this->db->get_where("ticket",["departement_id_from"=>$this->session->departement_id,"status"=>"selesai"])->num_rows();
        }
        $data = array(
            "response" => "success",
            'csrfHash' => $this->security->get_csrf_hash(),
            "msg"      => $cek,
        );
        return $data;
    }
}