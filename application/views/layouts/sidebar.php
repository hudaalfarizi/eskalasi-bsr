<?php
$status    = "";
$statistik = "";
$master    = "";
if ($this->permission->cek_permission('create_request_service') == FALSE && $this->permission->cek_permission('incoming_service_request') == FALSE && $this->permission->cek_permission('service_request_in_process') == FALSE  && $this->permission->cek_permission('service_request_open') == FALSE) {
	$status = 'hide';
}
if ($this->permission->cek_permission('all_request_service') == FALSE) {
	$statistik = 'hide';
}
if ($this->permission->cek_permission('permission') == FALSE && $this->permission->cek_permission('position') == FALSE && $this->permission->cek_permission('departement') == FALSE) {
	$master = 'hide';
}
?>
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf">
<div class="col-12 col-sm-4 col-md-3 mb-3">
	<div class="card" id="sidebar">
		<div class="card-body">
			<div class="card-title <?php echo $status ?>"><b>Status</b></div>
			<hr class="<?php echo $status ?>">
			<p class="card-text <?php echo $status ?>">
				<ul class="list-group">

					<?php
					if ($this->permission->cek_permission('create_request_service')) { ?>
						<a href="<?php echo site_url('buat_complain'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "buat_complain") ? "active" : ""; ?>">Create Request Service</li>
						</a>
					<?php } ?>

					<?php
					if ($this->permission->cek_permission('incoming_service_request')) { ?>
						<a href="<?php echo site_url('complain_masuk'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "complain_masuk") ? "active" : ""; ?>">Incoming Service Request<span style="float:right !important;color:success;" id="nmasuk"></span></li>
						</a>
					<?php } ?>

					<?php
					if ($this->permission->cek_permission('service_request_open')) { ?>
						<a href="<?php echo site_url('complain_open'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "complain_open") ? "active" : ""; ?>">Service Request Open<span style="float:right !important;color:#00cc00;" id="nopen"></span></li>
						</a>
					<?php } ?>

					<?php
					if ($this->permission->cek_permission('service_request_in_process')) { ?>
						<a href="<?php echo site_url('complain_proses'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "complain_proses") ? "active" : ""; ?>">Service Request in Process<span style="float:right !important;color:#cca300;" id="nproses"></span></li>
						</a>
					<?php } ?>

					<?php
					if ($this->permission->cek_permission('service_approval')) { ?>
						<a href="<?php echo site_url('complain_approve'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "complain_approve") ? "active" : ""; ?>">Service Approval<span style="float:right !important;" id="napprove"></span></li>
						</a>
					<?php } ?>

					<?php
					if ($this->permission->cek_permission('service_request_completed')) { ?>
						<a href="<?php echo site_url('complain_selesai'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "complain_selesai") ? "active" : ""; ?>">Service Request Completed<span style="float:right !important;" id="nselesai"></span></li>
						</a>
					<?php } ?>
				</ul>
			</p>
			<hr class="<?php echo $status ?>">
			<div class="card-title <?php echo $statistik ?>"><b>Building Service Satistik</b></div>
			<p class="card-text <?php echo $statistik ?>">
				<ul class="list-group">
				<?php
				if ($this->permission->cek_permission('all_request_service')) { ?>
					<a href="<?php echo site_url('all_request_service'); ?>">
						<li class="list-group-item <?php echo ($this->uri->segment('1') == "all_request_service") ? "active" : ""; ?>">All Request Service</li>
					</a>
				<?php } ?>
				</ul>
			</p>
			<hr class="<?php echo $statistik ?>">
			<div class="card-title intro <?php echo $master ?>"><b>Master Data</b></div>
			<hr class="<?php echo $master ?>">
			<p class="card-text <?php echo $master ?>">
				<ul class="list-group">
					<?php
					if ($this->permission->cek_permission('departement')) { ?>
						<a href="<?php echo site_url('departement'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "departement") ? "active" : ""; ?>">Departement</li>
						</a>
					<?php } ?>

					<?php
					if ($this->permission->cek_permission('position')) { ?>
						<a href="<?php echo site_url('position'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "position") ? "active" : ""; ?>">Position</li>
						</a>
					<?php } ?>

					<?php
					if ($this->permission->cek_permission('permission')) { ?>
						<a href="<?php echo site_url('permission'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "permission") ? "active" : ""; ?>">Permission</li>
						</a>
					<?php } ?>

					<?php
					if ($this->permission->cek_permission('problem_category')) { ?>
						<a href="<?php echo site_url('problem_category'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "problem_catgeory") ? "active" : ""; ?>">Problem Category</li>
						</a>
					<?php } ?>

					<?php
					if ($this->permission->cek_permission('user')) { ?>
						<a href="<?php echo site_url('user'); ?>">
							<li class="list-group-item <?php echo ($this->uri->segment('1') == "user") ? "active" : ""; ?>">User</li>
						</a>
					<?php } ?>
				</ul>
			</p>
			<div class="card-title intro"><b>Setting</b></div>
			<hr>
			<p class="card-text">
				<ul class="list-group">
					<a href="<?php echo site_url('edit_profile'); ?>">
						<li class="list-group-item <?php echo ($this->uri->segment('1') == "edit_profile") ? "active" : ""; ?>">Edit Profile</li>
					</a>
					<a href="<?php echo site_url('ubah_password'); ?>">
						<li class="list-group-item <?php echo ($this->uri->segment('1') == "ubah_password") ? "active" : ""; ?>">Ubah Password</li>
					</a>
					<a href="<?php echo site_url('logout'); ?>">
						<li class="list-group-item">Logout</li>
					</a>
				</ul>
			</p>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/articulate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js'); ?>"></script>
<script>
	$(document).ready(function() {
		notif_open();
		notif_masuk();
		notif_proses();
		notif_approve();
		notif_selesai();
		setInterval(function() {
			notif_open();
			notif_masuk();
			notif_proses();
			notif_approve();
			notif_selesai();
		}, 10000);
	});

	function notif_open() {
		var csrfName = $("#csrf").attr('name');
		var csrfHash = $("#csrf").val();
		$.ajax({
			url: "<?php echo site_url('notif_open'); ?>",
			dataType: "JSON",
			type: "POST",
			data: {
				[csrfName]: csrfHash
			},
			success: function(obj) {
				$("#nopen").text(obj.msg);
				$("#csrf").val(obj.csrfHash);
			}
		})
	}

	function notif_masuk() {
		var csrfName = $("#csrf").attr('name');
		var csrfHash = $("#csrf").val();
		$.ajax({
			url: "<?php echo site_url('notif_masuk'); ?>",
			dataType: "JSON",
			type: "POST",
			data: {
				[csrfName]: csrfHash
			},
			success: function(obj) {
				$("#nmasuk").text(obj.msg);
				var total = parseInt(obj.msg);
				if (total > 0) {
					$("#csrf").val(obj.csrfHash);
				}
			}
		})
	}

	function notif_proses() {
		var csrfName = $("#csrf").attr('name');
		var csrfHash = $("#csrf").val();
		$.ajax({
			url: "<?php echo site_url('notif_proses'); ?>",
			dataType: "JSON",
			type: "POST",
			data: {
				[csrfName]: csrfHash
			},
			success: function(obj) {
				$("#nproses").text(obj.msg);
				$("#csrf").val(obj.csrfHash);
			}
		})
	}

	function notif_approve() {
		var csrfName = $("#csrf").attr('name');
		var csrfHash = $("#csrf").val();
		$.ajax({
			url: "<?php echo site_url('notif_approve'); ?>",
			dataType: "JSON",
			type: "POST",
			data: {
				[csrfName]: csrfHash
			},
			success: function(obj) {
				$("#napprove").text(obj.msg);
				$("#csrf").val(obj.csrfHash);
			}
		})
	}

	function notif_selesai() {
		var csrfName = $("#csrf").attr('name');
		var csrfHash = $("#csrf").val();
		$.ajax({
			url: "<?php echo site_url('notif_selesai'); ?>",
			dataType: "JSON",
			type: "POST",
			data: {
				[csrfName]: csrfHash
			},
			success: function(obj) {
				$("#nselesai").text(obj.msg);
				$("#csrf").val(obj.csrfHash);
			}
		})
	}

	function bacot(total) {
		var synth = window.speechSynthesis;
		var voices = [];
		var toSpeak = new SpeechSynthesisUtterance(total + "complain online masuuuk");
		var selectedVoiceName = "Google Bahasa Indonesia";
		voices.forEach((voice) => {
			if (voice.name === selectedVoiceName) {
				toSpeak.voice = voice;
			}
		});
		synth.speak(toSpeak);
	};
</script>