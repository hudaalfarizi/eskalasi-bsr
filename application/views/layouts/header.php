<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
	<title>Building Service</title>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top mb-5">
		<a class="navbar-brand" href="<?php echo base_url('home');?>"><b>Building Service</b></a>

		<div class="author-session navbar-nav ml-5">
			<?php echo $this->permission->information_user()->name ." - ". $this->permission->information_user()->position_name . " " . $this->permission->information_user()->departement_name ;?>
		</div>

		<div class="author-session ml-auto">
			<?php echo $this->permission->date_indo(); ?>
		</div>
	</nav>
	<br>
	<div class="container-fluid mt-5">
		<div class="row">