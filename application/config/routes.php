<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'authorization/AuthorizationController';
$route['home']               = 'home/HomeController';
//authorization
$route['co/auth'] = 'authorization/AuthorizationController/cek_validation';
$route['logout']  = 'authorization/AuthorizationController/logout';
//buat complain
$route['buat_complain']    = 'create_complain/CreateComplainController';
$route['get_kategori']     = 'create_complain/CreateComplainController/get_kategori';
$route['co/buat_complain'] = 'create_complain/CreateComplainController/do_upload';
//complain masuk
$route['complain_masuk']                 = 'complain_masuk/ComplainMasukController';
$route['get_complain_masuk']             = 'complain_masuk/ComplainMasukController/data_complain_masuk';
$route['ambil_complain']                 = 'complain_masuk/ComplainMasukController/ambil_complain';
$route['complain_masuk/detail_complain'] = 'complain_masuk/ComplainMasukController/detail_complain';
//complain proses
$route['complain_proses']                 = 'complain_proses/ComplainProsesController';
$route['get_complain_proses']             = 'complain_proses/ComplainProsesController/data_complain_proses';
$route['complain_update_selesai']         = 'complain_proses/ComplainProsesController/update_selesai';
$route['complain_pending']                = 'complain_proses/ComplainProsesController/pending';
$route['complain_proses/detail_complain'] = 'complain_proses/ComplainProsesController/detail_complain';
//complain approve
$route['complain_approve']                 = 'complain_approve/ComplainApproveController';
$route['get_complain_approve']             = 'complain_approve/ComplainApproveController/data_complain_approve';
$route['complain_update_approve']          = 'complain_approve/ComplainApproveController/update_approve';
$route['complain_approve/detail_complain'] = 'complain_approve/ComplainApproveController/detail_complain';
//complain selesai
$route['complain_selesai']                 = 'complain_selesai/ComplainSelesaiController';
$route['get_complain_selesai']             = 'complain_selesai/ComplainSelesaiController/data_complain_selesai';
$route['complain_update_tutup']            = 'complain_selesai/ComplainSelesaiController/update_tutup';
$route['tutup_complain']                   = 'complain_selesai/ComplainSelesaiController/tutup_complain';
$route['complain_selesai/detail_complain'] = 'complain_selesai/ComplainSelesaiController/detail_complain';
//complain open
$route['complain_open']                    = 'complain_open/ComplainOpenController';
$route['get_complain_open']                = 'complain_open/ComplainOpenController/data_complain_open';
$route['complain_open/detail_complain']    = 'complain_open/ComplainOpenController/detail_complain';
//complain open
$route['all_request_service']              = 'all_request_service/AllRequestServiceController';
$route['pilih_berdasarkan_trace']          = 'all_request_service/AllRequestServiceController/pilih_berdasarkan';
$route['data_tracelog']                    = 'all_request_service/AllRequestServiceController/data_tracelog';
$route['get_all_request_service']          = 'all_request_service/AllRequestServiceController/data_all_request';
$route['all_request_service/detail_complain']    = 'all_request_service/AllRequestServiceController/detail_complain';
//notifikasi
$route['notif_open']    = 'NotifikasiController/notif_open';
$route['notif_masuk']   = 'NotifikasiController/notif_masuk';
$route['notif_proses']  = 'NotifikasiController/notif_proses';
$route['notif_approve'] = 'NotifikasiController/notif_approve';
$route['notif_selesai'] = 'NotifikasiController/notif_selesai';
//profile
$route['edit_profile']         = 'profile/ProfileController';
$route['update_profile']       = 'profile/ProfileController/update_profile';
$route['update_foto_profile']  = 'profile/ProfileController/update_foto_profile';
$route['ubah_password']        = 'profile/ProfileController/ubah_password';
$route['update_password']      = 'profile/ProfileController/update_password';
//master departement
$route['departement']         = 'departement/DepartementController';
$route['data_departement']    = 'departement/DepartementController/data_departement';
$route['add_departement']     = 'departement/DepartementController/add_departement';
$route['departement/delete_departement'] = 'departement/DepartementController/delete_departement';
//master position
$route['position']         = 'position/PositionController';
$route['data_position']    = 'position/PositionController/data_position';
$route['add_position']     = 'position/PositionController/add_position';
$route['update_position']  = 'position/PositionController/update_position';
$route['position/delete_position'] = 'position/PositionController/delete_position';
$route['position/edit_position']   = 'position/PositionController/edit_position';
//module
$route['permission']         = 'permission/PermissionController';
$route['pilih_berdasarkan']  = 'permission/PermissionController/pilih_berdasarkan';
$route['data_permission']    = 'permission/PermissionController/data_permission';
$route['select_module']      = 'permission/PermissionController/select_module';
$route['update_permission']  = 'permission/PermissionController/update_permission';
$route['permission/delete_permission']  = 'permission/PermissionController/delete_permission';
//user
$route['user']             = 'user/UserController';
$route['get_data_user']    = 'user/UserController/get_data_user';
$route['save_user']        = 'user/UserController/save_user';
$route['user/edit_user']   = 'user/UserController/edit_user';
$route['user/delete_user'] = 'user/UserController/delete_user';
$route['update_user']      = 'user/UserController/update_user';
//master problem category
$route['problem_category']         = 'problem_category/ProblemCategoryController';
$route['data_problem_category']    = 'problem_category/ProblemCategoryController/data_problem_category';
$route['problem_category/edit']	   = 'problem_category/ProblemCategoryController/edit_problem_category';
$route['update_problem_category']  = 'problem_category/ProblemCategoryController/update_problem_category';
$route['save_problem_category']    = 'problem_category/ProblemCategoryController/save_problem_category';
$route['problem_category/delete']  = 'problem_category/ProblemCategoryController/delete_problem_category';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
