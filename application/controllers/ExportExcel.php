<?php defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportExcel extends CI_Controller {
	public function __construct() {
        parent::__construct();
        if($this->permission->cek_permission('create_request_service')) {
            $this->load->model('ExportModel','model');
        } else {
            redirect('home');
        }
    }
	public function export_report() {
		$tipe   = $_GET['tipe'];
		$iddata = $_GET['iddata'];
		if($tipe == null OR $iddata == null) {
			echo "tipe data harus di isi";
		} else {
			$report = $this->model->export_report($tipe,$iddata);
			$x  = 2;
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setCellValue('A1', 'Ticket No');
			$sheet->setCellValue('B1', 'PIC');
			$sheet->setCellValue('C1', 'Date');
			$sheet->setCellValue('D1', 'Create');
			$sheet->setCellValue('E1', 'Approve');
			$sheet->setCellValue('F1', 'Execute');
			$sheet->setCellValue('G1', 'Pending');
			$sheet->setCellValue('H1', 'Finish');
			$sheet->setCellValue('I1', 'Manhours');
			foreach($report as $row)
			{
				$sheet->setCellValue('A'.$x, $row->ticket_code);	
				$sheet->setCellValue('B'.$x, $row->username);
				$sheet->setCellValue('C'.$x, $row->date);
				$sheet->setCellValue('D'.$x, $row->open);
				$sheet->setCellValue('E'.$x, $row->approve);
				$sheet->setCellValue('F'.$x, $row->execute);
				$sheet->setCellValue('G'.$x, $row->pending);
				$sheet->setCellValue('H'.$x, $row->finish);
				$sheet->setCellValue('I'.$x, $row->manhours);
				$x++;
			}
			$writer = new Xlsx($spreadsheet);
			$date   = date('Y-m-d');
			$filename = 'laporan-ticket_'.$date;
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
			header('Cache-Control: max-age=0');
			$writer->save('php://output');
		}
	}
}