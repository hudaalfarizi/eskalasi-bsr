<?php defined('BASEPATH') OR exit('No direct script access allowed');

class NotifikasiController extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model("Notifikasi","model");
    }
    function notif_open() {
        $data = $this->model->notif_open();
        echo json_encode($data);
    }
    function notif_masuk() {
        $data = $this->model->notif_masuk();
        echo json_encode($data);
    }
    function notif_proses() {
        $data = $this->model->notif_proses();
        echo json_encode($data);
    }
    function notif_approve() {
        $data = $this->model->notif_approve();
        echo json_encode($data);
    }
    function notif_selesai() {
        $data = $this->model->notif_selesai();
        echo json_encode($data);
    }
}