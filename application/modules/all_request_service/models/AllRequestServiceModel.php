<?php

class AllRequestServiceModel extends CI_Model {
    function get_datatables(){
		$site_url = site_url();
		$this->datatables->select("
					ticket.ticket_code,
					departement.departement_name,
					problem_category.problem_category_name,
                    profile.name,
					problem_category.priority,
					CONCAT(
						'<a href=','''','$site_url','/all_request_service/detail_complain?id=',MD5(ticket.ticket_id),'''','class=','''','btn btn-sm btn-success','''','>','lihat</a> '
					) AS action,

		");
		$this->datatables->from("ticket");
		$this->datatables->join("profile", "profile.user_id=ticket.user_id", "left");
		$this->datatables->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left");
		$this->datatables->join("departement", "departement.departement_id=ticket.departement_id_from", "left");
		if($this->session->departement_type == 'user') {
			$this->datatables->where("ticket.departement_id_from",$this->session->departement_id);
		} elseif($this->session->departement_type == 'maintenance') {
			$this->datatables->where("ticket.departement_id_destination",$this->session->departement_id);
		}
		$this->datatables->where("ticket.status","closed");
		return $this->datatables->generate();
	}
	function detail_complain($id){
		$data = $this->db->select("
								ticket.ticket_id,
								ticket.ticket_code,
								ticket.departement_id_from,
								departement.departement_name,
								problem_category.problem_category_name,
								profile.name,
								ticket.description,
								ticket.created_at,
								ticket.updated_at,
								ticket.image,
								problem_category.priority,
								act.start_at,
								act.end_at,
								act.root_cause,
								act.handling
						 ")
						 ->from("ticket")
						 ->join("activity_assignment act", "act.ticket_id=ticket.ticket_id", "left")
						 ->join("profile", "profile.user_id=ticket.user_id", "left")
						 ->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left")
						 ->join("departement", "departement.departement_id=ticket.departement_id_from", "left")
						 ->where("md5(ticket.ticket_id)",$id)
						 ->get()->row();
		return $data;
	}
	function tutup_complain($ticket) {
		$cek = $this->db->get_where("ticket",["ticket_id"=>$ticket,"status"=>"selesai"])->num_rows();
		if($cek > 0) {
			$this->db->where("ticket_id",$ticket);
			$update = $this->db->update("ticket",["status"=>"closed"]);
			if($update) {
				$data = array(
					"response" => "success",
					"msg"      => "berhasil tutup complain",
					"url"      => site_url("complain_selesai"),
				);
			} else {
				$data = array(
					"response" => "failed",
					"msg"      => "gagal tutup complain",
					"url"      => site_url("complain_selesai"),
				);
			}
		} else {
			$data = array(
				"response" => "failed",
				"msg"      => "sudah tutup complain",
				"url"      => site_url("complain_selesai"),
			);
		}
		return $data;
	}
	function pilih_berdasarkan($id) {
		if($id == 1) {
			$query = $this->db->select('
									t.ticket_id,
									t.ticket_code,
							  ')
							  ->from('ticket t')
							  ->get()->result();
			$data = array(
                'response' => 'success',
                'msg'      => $query,
                'csrfHash' => $this->security->get_csrf_hash(),
            );
		} elseif ($id == 2) {
			$query = $this->db->select('
									u.user_id,
									u.username as pic_name,
							  ')
							  ->from('user u')
							  ->get()->result();
			$data = array(
                'response' => 'success',
                'msg'      => $query,
                'csrfHash' => $this->security->get_csrf_hash(),
            );
		}
		return $data;
	}

	function data_tracelog($tipe,$id) {
		$site_url = site_url();
		$this->datatables->select("
					t.ticket_code,
					u.username,
					tr.date,
					tr.open,
					tr.approve,
					tr.execute,
					tr.pending,
					tr.finish,
					tr.manhours

		");
		$this->datatables->from("tracelog_ticket tr");
		$this->datatables->join("ticket t", "tr.ticket_id=t.ticket_id", "left");
		$this->datatables->join("user u", "tr.pic=u.user_id", "left");
		if($tipe == 1) {
			if($id != 0) {
				$this->datatables->where('tr.ticket_id',$id);
			}
		} elseif ($tipe == 2) {
			$this->datatables->where("tr.pic",$id);
		}
		$this->db->order_by('t.ticket_code','desc');
		$this->db->order_by('tr.tracelog_ticket_id','asc');
		return $this->datatables->generate();
	}
}