<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AllRequestServiceController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->permission->cek_permission('all_request_service')) {
			$this->load->model('AllRequestServiceModel','model');
		} else {
			redirect('home');
		}	
	}
	public function index()
	{
		$data = array(
            'content' => 'trace',
        );
        $this->load->view('layouts/template',$data);
	}
	public function data_all_request() {
		header('Content-Type: application/json');
		echo $this->model->get_datatables();
	}
	public function detail_complain() {
		$id     = $_GET['id'];
		$result = $this->model->detail_complain($id);
		$data   = array(
			'content' => 'detail_complain',
			'result'  => $result,
		);
		$this->load->view('layouts/template',$data);
	}
	public function tutup_complain(){
		$ticket = $this->input->post('ticket');
		$data   = $this->model->tutup_complain($ticket);
		echo json_encode($data);
	}
	public function pilih_berdasarkan() {
		$id   = $this->input->post('id');
		$data = $this->model->pilih_berdasarkan($id);
		echo json_encode($data);
	}
	public function data_tracelog() {
		header('Content-Type: application/json');
		$tipe = $this->input->post('tipe');
		$id   = $this->input->post('iddata');
		echo $this->model->data_tracelog($tipe,$id);
	}
}
