<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/css/select2.css">
<div class="col-12 col-sm-9">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Statistik</b></h5>
			<hr>
			<br>

			<div class="row mb-1">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
					value="<?php echo $this->security->get_csrf_hash(); ?>" class="csrf">
				<div class="col-4 mb-1">
					<input type="hidden" id="hideid">
					<select name="pilih_data" id="pilih_data" class="form-control">
						<option value="" disbled="disabled">Cari Berdasarkan</option>
						<option value="1">Ticket</option>
						<option value="2">User</option>
					</select>
				</div>

				<div class="col-4 mb-1">
					<input type="hidden" id="hideid2">
					<select name="pilih_berdasarkan" id="pilih_berdasarkan" class="form-control"
						data-source="<?php echo site_url('pilih_berdasarkan_trace') ?>">
					</select>
				</div>

				<div class="col-4 mb-1">
					<button class="btn btn-success" id="cari">cari</button> | <button class="btn btn-info" id="export">export</button> 
				</div>
			</div>
			<hr>
			<div class="table-responsive mt-2">
				<table id="table-all" data-source="<?php echo site_url("data_tracelog"); ?>"
					class="table table-stripped">
					<thead>
						<th>ticket</th>
						<th>pic</th>
						<th>date</th>
						<th>create</th>
						<th>approve</th>
						<th>execute</th>
						<th>pending</th>
						<th>finish</th>
						<th>manhours</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/general.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dt.css') ?>">
<script src="<?php echo base_url('assets/js/dt.js'); ?>"></script>
<script src="<?php echo base_url() ?>assets/plugins/select2/js/select2.js"></script>
<script>
	$(document).ready(function () {
		$("#pilih_berdasarkan").prop("disabled", true);
		$("#pilih_berdasarkan").select2();
	});
	$("#pilih_data").on("change", function () {
		$("#pilih_berdasarkan").prop("disabled", false);
		$("#pilih_berdasarkan").empty();
		var id = $(this).val();
		var csrfName = $(".csrf").attr('name');
		var csrfHash = $(".csrf").val();
		$.ajax({
			url: $("#pilih_berdasarkan").attr("data-source"),
			type: "POST",
			dataType: "JSON",
			data: {
				id: id,
				[csrfName]: csrfHash
			},
			success: function (obj) {
				if (id == 1) {
					$("#pilih_berdasarkan").append("<option value='0'>ALL</option>");
					$.each(obj.msg, function (i, val) {
						$("#pilih_berdasarkan").append("<option value='" + val.ticket_id + "'>" + val.ticket_code + "</option>");
					});
				} else if (id == 2) {
					$.each(obj.msg, function (i, val) {
						$("#pilih_berdasarkan").append("<option value='" + val.user_id + "'>" +
							val.pic_name + "</option>");
					});
				}
				$(".csrf").val(obj.csrfHash);
			}
		});
	});
	$("#export").on("click",function(){
		var tipe   = $("#pilih_data").val();
		var iddata = $("#pilih_berdasarkan").val();
		var page   = "<?php echo site_url('ExportExcel/export_report') ?>" + "?tipe=" + tipe + "&iddata=" + iddata ;
		window.open(page); 
	});
	$("#cari").on("click", function () {
		var tipe   = $("#pilih_data").val();
		var iddata = $("#pilih_berdasarkan").val();
		var url    = $('#table-all').attr('data-source');
		var csrfName = $(".csrf").attr('name');
		var csrfHash = $(".csrf").val();

		//save data
		$("#hideid").val(tipe);
		$("#hideid2").val(iddata);
		//show add button
		$("#add").show();

		$('#table-all').dataTable().fnDestroy();

		$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		var table = $("#table-all").dataTable({
			initComplete: function () {
				var api = this.api();
				$('#mytable_filter input')
					.off('.DT')
					.on('input.DT', function () {
						api.search(this.value).draw();
					});
			},
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {
				"url": url,
				"type": "POST",
				"data": {
					iddata: iddata,
					tipe: tipe,
					[csrfName]: csrfHash
				}
			},
			columns: [{
					"data": "ticket_code"
				},
				{
					"data": "username"
				},
				{
					"data": "date"
				},
				{
					"data": "open"
				},
				{
					"data": "approve"
				},
				{
					"data": "execute"
				},
				{
					"data": "pending"
				},
				{
					"data": "finish"
				},
				{
					"data": "manhours"
				}
			],
			columnDefs: [{
				"targets": [0],
				"orderable": false,
				"searchable": false,
			}],
			order: [],
			rowCallback: function (row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				$('td:eq(0)', row).html();
			}

		});
	});

	$("#add").on("click", function () {
		var tipe = $("#hideid").val();
		var iddata = $("#hideid2").val();
		var csrfName = $(".csrf").attr('name');
		var csrfHash = $(".csrf").val();
		$.ajax({
			url: "<?php echo site_url('select_module') ?>",
			data: {
				tipe: tipe,
				iddata: iddata,
				[csrfName]: csrfHash,
			},
			type: "POST",
			dataType: "JSON",

			success: function (obj) {
				$("#module").modal("show");
				$("#modul").empty();
				$.each(obj.msg, function (i, val) {
					$("#modul").append("<option value='" + val.module_id + "'>" + val
						.module_name + "</option>");
				});
				$(".csrf").val(obj.csrfHash);
			}
		});
	})
	$("#simpan").on("click", function () {
		var tipe = $("#hideid").val();
		var iddata = $("#hideid2").val();
		var csrfName = $(".csrf").attr('name');
		var csrfHash = $(".csrf").val();
		var idmodul = $("#modul").val();

		$.ajax({
			url: "<?php echo site_url('update_permission') ?>",
			data: {
				tipe: tipe,
				iddata: iddata,
				idmodul: idmodul,
				[csrfName]: csrfHash,
			},
			type: "POST",
			dataType: "JSON",

			success: function (data) {
				if (data.response == 'success') {
					Swal.fire({
						icon: 'success',
						title: 'Selamat !',
						text: data.msg,
						type: "success"
					}).then(function () {
						get_table();
						$("#module").modal("hide");
					});
				} else if (data.response == 'failed') {

					$('.information').css('display', 'block');
					$('.information').addClass('alert alert-danger');
					$('.information').html(data.msg);
					$("#csrf").val(data.csrfHash);
					Swal.fire({
						icon: 'error',
						title: 'Ada Kesalahan !',
						text: data.msg,
						type: "error"
					});
				}
			}
		});
	})
	$(document).on('click', '.delete_permission', function (e) {
		var id = $(this).attr('data-source');
		var url = $(this).attr('data-uri');
		var csrfName = $("#csrf").attr('name');
		var csrfHash = $("#csrf").val();
		Swal.fire({
			title: 'Hapus Data',
			text: "apakah anda yakin?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'JSON',
					data: {
						id: id,
						[csrfName]: csrfHash,
					},

					success: function (data) {
						if (data.response == 'success') {
							Swal.fire({
								icon: 'success',
								title: 'Selamat !',
								text: data.msg,
								type: "success"
							}).then(function () {
								get_table();
							});
						} else if (data.response == 'failed') {

							$('.information').css('display', 'block');
							$('.information').addClass('alert alert-danger');
							$('.information').html(data.msg);
							$("#csrf").val(data.csrfHash);
							Swal.fire({
								icon: 'error',
								title: 'Ada Kesalahan !',
								text: 'terjadi kesalahan',
								type: "error"
							});
						}
					}
				})
			}
		})

	});

	function get_table() {
		var tipe = $("#pilih_data").val();
		var iddata = $("#pilih_berdasarkan").val();
		var url = $('#table-all').attr('data-source');
		var csrfName = $(".csrf").attr('name');
		var csrfHash = $(".csrf").val();

		//save data
		$("#hideid").val(tipe);
		$("#hideid2").val(iddata);
		//show add button
		$("#add").show();

		$('#table-all').dataTable().fnDestroy();

		$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		var table = $("#table-all").dataTable({
			initComplete: function () {
				var api = this.api();
				$('#mytable_filter input')
					.off('.DT')
					.on('input.DT', function () {
						api.search(this.value).draw();
					});
			},
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {
				"url": url,
				"type": "POST",
				"data": {
					iddata: iddata,
					tipe: tipe,
					[csrfName]: csrfHash
				}
			},
			columns: [{
					"data": "module_name"
				},
				{
					"data": "action"
				}
			],
			columnDefs: [{
				"targets": [1],
				"orderable": false,
				"searchable": false,
			}],
			order: [],
			rowCallback: function (row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				$('td:eq(0)', row).html();
			}

		});
	}

</script>
