<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if($this->session->is_login === TRUE) {
            $this->load->model('ProfileModel','model');
        } else {
            redirect('home');
        }
    }
	public function index()
	{
		$data = array(
            'content'     => 'profile',
            'departement' => '',
        );
        $this->load->view('layouts/template',$data);
    }
    public function update_foto_profile() {
        if(!empty($_FILES['file']['name'])) {
            $config['upload_path']   ="./assets/images/profile";
            $config['allowed_types'] ='gif|jpg|png|jpeg';
            $config['encrypt_name']  = TRUE;
            
            $this->load->library('upload',$config);
            if($this->upload->do_upload('file')){
                $data = array('upload_data' => $this->upload->data());
                $image  = $data['upload_data']['file_name'];
                $user_id= $this->input->post('user_id');
                $data   = $this->model->simpan_upload($image,$user_id);
            } else {
                $data = array(
                    'response' => 'failed',
                    'msg'      => $this->upload->display_errors(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                );
            }
        } else {
            $data = array(
                "response"  => "success",
                "csrfHash"  => $this->security->get_csrf_hash(),
            );
        }
        echo json_encode($data);
    }
    public function update_profile() {
        $this->form_validation->set_rules(
                                'name',
                                'Nama',
                                'required',
                                ['required'=>'Nama Harus Di Isi']
                            );
        $this->form_validation->set_rules(
                                'email',
                                'Email',
                                'required|valid_email',
                                ['required'=>'Email Harus Di Isi']
                            );
        $this->form_validation->set_rules(
                                'phone',
                                'No Hp',
                                'required|numeric|min_length[11]|max_length[13]',
                                array(
                                    'required'   => 'phone harus di isi',
                                    'min_length' => 'minimal phone 11 angka',
                                    'max_length' => 'maximal phone 13 angka',
                                )
                            );
        if($this->form_validation->run() === FALSE) {
            $data = array(
                'response' => 'failed',
                'msg'      => validation_errors(),
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        } else {
            $name    = $this->input->post('name');
            $email   = $this->input->post('email');
            $phone   = $this->input->post('phone');
            $user_id = $this->input->post('user_id');
            $data    = $this->model->simpan_data($name,$email,$phone,$user_id);
        }
        echo json_encode($data);
    }
    public function ubah_password() {
        $data = array(
            'content'     => 'ubah_password',
            'departement' => '',
        );
        $this->load->view('layouts/template',$data);
    }
    public function update_password() {
        $this->form_validation->set_rules(
                                    'password_lama',
                                    'Password Lama',
                                    'required',
                                    array(
                                        'required'   => 'Password Lama harus di isi',
                                    )
                                );
        $this->form_validation->set_rules(
                                    'password_baru',
                                    'Password Baru',
                                    'required|min_length[8]',
                                    array(
                                        'required'   => 'Password Baru harus di isi',
                                        'min_length' => 'minimal password 8 karakter',
                                    )
                                );
        $this->form_validation->set_rules(
                                    'konfirmasi_password',
                                    'Konfirmasi Password',
                                    'required|min_length[8]',
                                    array(
                                        'required'   => 'Password Baru harus di isi',
                                        'min_length' => 'minimal password 8 karakter',
                                    )
                                );
        if($this->form_validation->run() === FALSE ) {
            $data = array(
                'response' => 'failed',
                'msg'      => validation_errors(),
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        } else {
            $pass_lama = $this->input->post('password_lama');
            $pass_baru = $this->input->post('password_baru');
            $konf_pass = $this->input->post('konfirmasi_password');
            if($pass_baru !== $konf_pass) {
                $data = array(
                    'response' => 'failed',
                    'msg'      => 'konfirmasi password tidak sesuai',
                    'csrfHash' => $this->security->get_csrf_hash(),
                );
            } else {
                $data = $this->model->update_password($pass_lama,$pass_baru);
            }
        }
        echo json_encode($data);
    }
}
