<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf1">
<div class="col-12 col-sm-8 col-md-9">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Profile</b></h5>
			<hr>
			<div class="information"></div>
			<br>
			<div class="row">
				<div class="col-12 col-sm-4">
					<img id="img" src="<?php echo base_url('assets/images/profile/'.$this->permission->information_user()->profile_picture);?>" alt="..." class="img-thumbnail">
					<input type="file" id="file"  accept="image/x-png,image/gif,image/jpeg" data-uri="<?php echo site_url('update_foto_profile');?>">
					<div class="loading"></div>
				</div>
				<div class="col-12 col-sm-7">
					<form class="form-post" data-uri="<?php echo site_url('update_profile');?>">
					<input type="hidden" name="user_id" value="<?php echo $this->session->user_id?>" id="user_id">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf">
						<label for="name">Name : </label>
						<input id="name" name="name" type="text" class="form-control mb-3" value="<?php echo $this->permission->information_user()->name;?>">
						<label for="email">Email : </label>
						<input id="email" name="email" type="email" class="form-control mb-3" value="<?php echo $this->permission->information_user()->email;?>"
						<label for="phone">Phone : </label>
						<input id="phone" name="phone" type="number" class="form-control mb-3" value="<?php echo $this->permission->information_user()->phone;?>">
						<input type="submit" name="submit" class="btn btn-sm btn-success" value="Simpan">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
