<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf1">
<div class="col-12 col-sm-8 col-md-9">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Ubah Password</b></h5>
			<hr>
			<div class="information"></div>
			<br>
			<div class="row">
				<div class="col-12 col-sm-12">
					<form class="form-post" data-uri="<?php echo site_url('update_password');?>">
					<input type="hidden" name="user_id" value="<?php echo $this->session->user_id?>" id="user_id">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf">
						<label for="name">Password Lama : </label>
						<input id="password_lama" name="password_lama" type="password" class="form-control mb-3">
						<label for="email">Password Baru : </label>
						<input id="password_baru" name="password_baru" type="password" class="form-control mb-3">
						<label for="phone">Konfirmasi Password Baru : </label>
						<input id="konfirmasi_password" name="konfirmasi_password" type="password" class="form-control mb-3">
						<div class="key_pass"></div>
						<br>
						<input type="submit" name="submit" class="btn btn-sm btn-success" value="Simpan">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
<script>
	$("#konfirmasi_password").keyup(function(){
		var k = $("#konfirmasi_password").val();
		var p = $("#password_baru").val();
		if(k !== p) {
			$('.key_pass').css('display','block');
			$('.key_pass').addClass('alert alert-danger');
			$(".key_pass").text("password tidak sesuai");
		} else {
			$(".key_pass").removeClass("alert alert-danger");
			$(".key_pass").empty();
		}
	})
</script>
