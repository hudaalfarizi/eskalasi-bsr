<?php

class ProfileModel extends CI_Model {
    function simpan_upload($image,$user_id) {
        //unlink
        $gambar = $this->db->get_where("profile",["user_id"=>$user_id])->row();
        if($gambar->profile_picture !== "") {
            unlink('./assets/images/profile/'.$gambar->profile_picture);
        }
        $this->db->where("user_id",$user_id);
        $update=$this->db->update("profile",["profile_picture"=>$image]);
        if($update) {
            $data = array(
                "response" => "success",
                "msg"      => "berhasil update data",
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        } else {
            $data = array(
                "response" => "failed",
                "msg"      => "ada kesalahan saat update data",
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        }
        return $data;
    }
    function simpan_data($name,$email,$phone,$user_id) {
        $cek_email = $this->db->get_where('profile',['email'=>$email,'user_id'=>$user_id])->num_rows();
        if($cek_email == 1) {
            $insert = array(
                'name'    => $name,
                'email'   => $email,
                'phone'   => $phone,
            );
            $this->db->where('user_id',$user_id);
            $update=$this->db->update('profile',$insert);
            if($update) {
                $data = array(
                    'response'  => 'success',
                    'msg'       => 'berhasil update data',
                    'url'       => site_url('edit_profile'),
                    'csrfHash'  => $this->security->get_csrf_hash(),
                );
            } else {
                $data = array(
                    'response'  => 'failed',
                    'msg'       => 'terjadi kesalahan saat edit profile',
                    'url'       => site_url('edit_profile'),
                    'csrfHash'  => $this->security->get_csrf_hash(),
                );
            }
        } else {
            $this->form_validation->set_rules(
                'email',
                'Email',
                'required|valid_email|is_unique[profile.email]',
                ['required'=>'Email Harus Di Isi']
            );
            if($this->form_validation->run() === FALSE) {
                $data = array(
                    'response' => 'failed',
                    'msg'      => validation_errors(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                );
            } else {
                $insert = array(
                    'name'    => $name,
                    'email'   => $email,
                    'phone'   => $phone,
                );
                $this->db->where('user_id',$user_id);
                $update=$this->db->update('profile',$insert);
                if($update) {
                    $data = array(
                        'response'  => 'success',
                        'msg'       => 'berhasil update data',
                        'url'       => site_url('edit_profile'),
                        'csrfHash'  => $this->security->get_csrf_hash(),
                    );
                } else {
                    $data = array(
                        'response'  => 'failed',
                        'msg'       => 'terjadi kesalahan saat edit profile',
                        'url'       => site_url('edit_profile'),
                        'csrfHash'  => $this->security->get_csrf_hash(),
                    );
                }
            }
        }
        return $data;
    }
    function update_password($pass_lama,$pass_baru) {
        $cek = $this->db->get_where("user",["password"=>sha1($pass_lama),"user_id"=>$this->session->user_id])->num_rows();
        if($cek > 0) {
            $this->db->where("user_id",$this->session->user_id);
            $update=$this->db->update("user",["password"=>sha1($pass_baru)]);
            if($update) {
                $data = array(
                    "response" => "success",
                    "msg"      => "berhasil update data",
                    'url'      => site_url("ubah_password"),
                    'csrfHash' => $this->security->get_csrf_hash(),
                );
            } else {
                $data = array(
                    "response" => "failed",
                    "msg"      => "ada kesalahan saat update data",
                    'csrfHash' => $this->security->get_csrf_hash(),
                );
            }
            return $data;
        } else {
            $data = array(
                "response" => "failed",
                "msg"      => "password lama salah",
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        }
        return $data;
    }
}
