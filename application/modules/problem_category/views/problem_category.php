<div class="col-12 col-sm-6">
	<div class="information"></div>
		<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Problem Category</b></h5>
			<hr>	
			<br>
			<div class="table-responsive">
				<table id="table-all" data-source="<?php echo site_url("data_problem_category");?>">
					<thead>
						<th>Problem Category Name</th>
						<th>Departement Name</th>
						<th>Priority</th>
						<th>Action</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="col-12 col-sm-3">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Add Problem Category</b></h5>
			<hr>	
			<br>
			<form class="form-post" data-uri="<?php echo site_url('save_problem_category');?>">
				
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf">
				<input type="text" name="problem_category_name" class="form-control mb-3" placeholder="Problem Category Name">
				<select name="departement" id="type" class="form-control mb-3">
					<option disabled="disabled" selected="selected">Departement</option>
					<?php 
						foreach($departement as $row) {
							echo "<option value='" .$row->departement_id. "'> " .$row->departement_name. " </option>";
						}
					?>
				</select>
				<select name="priority" id="priority" class="form-control mb-3">
					<option disabled="disabled" selected="selected">Prioritas</option>
					<option value="hight">hight</option>
					<option value="medium">medium</option>
				</select>
				<input type="submit" name="submit" value='simpan' class="form-control">
			</form>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Position</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<form class="form-post" data-uri="<?php echo site_url('update_problem_category') ?>">
        <p>
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" class="csrf">	
			<input type="hidden" name="problem_category_id" id="problem_category_id">
			<input type="text" name="problem_category_name" id="problem_category_name" class="form-control mb-3">
			<select name="departement" id="departement" class="form-control mb-3">
					<option value="" id="departement_edit"></option>
					<?php 
						foreach($departement as $row) {
							echo "<option value='" .$row->departement_id. "'> " .$row->departement_name. " </option>";
						}
					?>
			</select>
			<select name="priority" class="form-control">
				<option id="priority_edit"></option>
				<option value="hight">hight</option>
				<option value="medium">medium</option>
			</select>
		</p>
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
	  </form>
</div>

<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dt.css')?>">
<script src="<?php echo base_url('assets/js/dt.js');?>"></script>
<script>
$( document ).ready(function() {
    data_table();
});
function data_table()
{
	var url     = $('#table-all').attr('data-source');
	var token   = "<?php echo $this->security->get_csrf_hash(); ?>";
	var csrfname= "<?php echo $this->security->get_csrf_token_name(); ?>";

	$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
	{
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};

	var table = $("#table-all").dataTable({
		initComplete: function() {
			var api = this.api();
			$('#mytable_filter input')
				.off('.DT')
				.on('input.DT', function() {
					api.search(this.value).draw();
			});
		},
			oLanguage: {
			sProcessing: "loading..."
		},
			processing: true,
			serverSide: true,
			ajax: {
				"url": url,
				"type": "POST",
				"data":{[csrfname]:token}
				},
				  columns: [
					  {"data": "problem_category_name"},
					  {"data": "departement_name"},
					  {"data": "priority"},
					  {"data": "action"}
				],
			columnDefs: [{
					"targets" : [3],
					"orderable": false,
					"searchable": false,
				  }],
			order: [],
		rowCallback: function(row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			$('td:eq(0)', row).html();
		}

	});
}

$(document).on('click', '.edit_problem', function (e) {
	var id  = $(this).attr('data-source');
	var url = $(this).attr('data-uri');
	var csrfName       = $("#csrf").attr('name');
	var csrfHash       = $("#csrf").val();
	$(".modal").modal("show");
	$.ajax({
		url      : url,
		data     : {id:id,[csrfName]:csrfHash},
		type     : "POST",
		dataType : "JSON",

		success  : function(obj) {
			$(".modal").modal("show");
			$("#problem_category_id").val(obj.msg.problem_category_id);
			$("#problem_category_name").val(obj.msg.problem_category_name);
			$("#departement_edit").text(obj.msg.departement_name);
			$("#departement_edit").val(obj.msg.departement_id);
			$("#priority_edit").val(obj.msg.priority);
			$("#priority_edit").text(obj.msg.priority);
			$(".csrf").val(obj.csrfHash);
		}
	})
	
});
</script>
