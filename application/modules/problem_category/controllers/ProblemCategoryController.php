<?php defined('BASEPATH') or exit ('No direct Script Access Allowed');

class ProblemCategoryController extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if($this->permission->cek_permission('problem_category')) {
			$this->load->model('ProblemCategoryModel','model');
		} else {
			redirect('home');
		}
    }
    public function index() {
        $data = array(
            'content'          => 'problem_category',
            'departement'      => $this->model->get_departement(),
        );
        $this->load->view('layouts/template',$data);
    }
    public function data_problem_category() {
        header('Content-Type: application/json');
		echo $this->model->get_datatables();
    }
    public function save_problem_category() {
		$this->form_validation->set_rules(
			'problem_category_name',
			'Nama Problem',
			'required',
			array('required'=>'Nama Problem wajib di isi')
		);
		$this->form_validation->set_rules(
				'departement',
				'Nama Departement',
				'required',
				array('required'=>'departement wajib di isi')
		);
		$this->form_validation->set_rules(
				'priority',
				'Prioritas',
				'required',
				array('required'=>'Prioritas wajib di isi')
		);
		if($this->form_validation->run() === FALSE) {
			$data = array(
				'response' => 'failed',
				'msg'      => validation_errors(),
				'csrfHash' => $this->security->get_csrf_hash(),
			);
		} else {
			$problem_name = $this->input->post('problem_category_name');
			$departement  = $this->input->post('departement');
			$priority     = $this->input->post('priority');
			$data = $this->model->save_problem_category($problem_name,$departement,$priority);
		}
		echo json_encode($data);
	}
    public function delete_problem_category() {
		$data = $this->model->hapus();
		echo json_encode($data);
	}
	public function edit_problem_category() {
		$data = $this->model->edit_problem_category();
		echo json_encode($data);
	}
	public function update_problem_category() {
		$this->form_validation->set_rules(
			'problem_category_name',
			'Nama Problem',
			'required',
			array('required'=>'Nama Problem wajib di isi')
		);
		$this->form_validation->set_rules(
				'departement',
				'Nama Departement',
				'required',
				array('required'=>'departement wajib di isi')
		);
		$this->form_validation->set_rules(
				'priority',
				'Prioritas',
				'required',
				array('required'=>'Prioritas wajib di isi')
		);
		if($this->form_validation->run() === FALSE) {
			$data = array(
				'response' => 'failed',
				'msg'      => validation_errors(),
				'csrfHash' => $this->security->get_csrf_hash(),
			);
		} else {
			$id           = $this->input->post('problem_category_id');
			$problem_name = $this->input->post('problem_category_name');
			$departement  = $this->input->post('departement');
			$priority     = $this->input->post('priority');
			$data = $this->model->update_problem_category($id,$problem_name,$departement,$priority);
		}
		echo json_encode($data);
	}
}