<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ComplainMasukController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->permission->cek_permission('incoming_service_request')) {
			$this->load->model('ComplainMasukModel','model');
		} else {
			redirect('home');
		}		
	}
	public function index()
	{
		$data = array(
            'content' => 'data_complain',
        );
        $this->load->view('layouts/template',$data);
	}
	public function data_complain_masuk() {
		header('Content-Type: application/json');
		echo $this->model->get_datatables();
	}
	public function detail_complain() {
		$id     = $_GET['id'];
		$result = $this->model->detail_complain($id);
		$data   = array(
			'content' => 'detail_complain',
			'result'  => $result,
		);
		$this->load->view('layouts/template',$data);
	}
	public function ambil_complain() {
		$ticket = $this->input->post('ticket');
		$data   = $this->model->ambil_complain($ticket);
		echo json_encode($data);
	}
}
