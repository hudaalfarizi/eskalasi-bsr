<?php

class ComplainMasukModel extends CI_Model {
    function get_datatables(){
		$site_url = site_url();
		$this->datatables->select("
					ticket.ticket_code,
					departement.departement_name,
					problem_category.problem_category_name,
                    profile.name,
                    problem_category.priority,
                    CONCAT(
						'<a href=','''','$site_url','/complain_masuk/detail_complain?id=',MD5(ticket.ticket_id),'''','class=','''','btn btn-sm btn-success','''','>','lihat</a> '
					) AS action,

		");
		$this->datatables->from("ticket");
		$this->datatables->join("profile", "profile.user_id=ticket.user_id", "left");
		$this->datatables->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left");
		$this->datatables->join("departement", "departement.departement_id=ticket.departement_id_from", "left");
		$this->datatables->where("ticket.departement_id_destination",$this->session->departement_id);
		$this->datatables->where("(ticket.status='open')");
		return $this->datatables->generate();
	}
	function detail_complain($id){
		$data = $this->db->select("
								ticket.ticket_id,
								ticket.ticket_code,
								departement.departement_name,
								problem_category.problem_category_name,
								profile.name,
								ticket.description,
								ticket.created_at,
								ticket.updated_at,
								ticket.image,
								problem_category.priority,
						 ")
						 ->from("ticket")
						 ->join("profile", "profile.user_id=ticket.user_id", "left")
						 ->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left")
						 ->join("departement", "departement.departement_id=ticket.departement_id_from", "left")
						 ->where("md5(ticket.ticket_id)",$id)
						 ->get()->row();
		return $data;
	}
	function ambil_complain($ticket) {
		$cek_ticket  = $this->db->get_where('ticket',['ticket_id'=>$ticket,'status'=>'open']);
		$cek_pending = $this->db->get_where('ticket',['ticket_id'=>$ticket,'status'=>'pending']);
		if($cek_ticket->num_rows() > 0) {
			$assignment = array(
				"ticket_id" => $ticket,
				"start_at"  => date('Y-m-d H:i'),
				"created_by"=> $this->session->user_id,
			);
			$date     = $cek_ticket->row()->created_at;
			$compare  = strtotime($date);
			$tgl      = date("Y-m-d", $compare);
			$now      = date("Y-m-d");
			$this->db->trans_start();
				$this->db->where("ticket_id",$ticket);
				$this->db->update("ticket",["status"=>"diproses","teknisi"=>$this->session->user_id]);
				$this->db->insert("activity_assignment",$assignment);
				if($tgl === $now) {
					$this->db->where("ticket_id",$ticket);
					$this->db->update("tracelog_ticket",["approve"=>date("H:i"),"execute"=>date("H:i"),"pic"=>$this->session->user_id]);
				} else {
					$tracelog = array(
						"ticket_id" => $ticket,
						"pic"       => $this->session->user_id,
						"date"      => date('Y-m-d'),
						"approve"   => date("H:i"),
						"execute"   => date("H:i"),
					);
					$this->db->insert("tracelog_ticket",$tracelog);
				}
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				$data = array(
					"response" => "failed",
					"msg"      => "complain gagal di ambil",
					'csrfHash' => $this->security->get_csrf_hash(),
					"url"      => site_url("complain_proses"),
				);
			} else {
				$data = array(
					"response" => "success",
					"msg"      => "anda berhasil ambil complain",
					'csrfHash' => $this->security->get_csrf_hash(),
					"url"      => site_url("complain_proses"),
				);
			}
		} elseif ($cek_pending->num_rows()>0) {
			$assignment = array(
				"ticket_id" => $ticket,
				"start_at"  => date('Y-m-d H:i'),
				"created_by"=> $this->session->user_id,
			);
			$this->db->trans_start();
				$this->db->where("ticket_id",$ticket);
				$this->db->update("ticket",["status"=>"diproses","teknisi"=>$this->session->user_id]);
				$this->db->insert("activity_assignment",$assignment);
				$tracelog = array(
					"ticket_id" => $ticket,
					"pic"       => $this->session->user_id,
					"date"      => date('Y-m-d'),
					"execute"   => date("H:i"),
				);
				$this->db->insert("tracelog_ticket",$tracelog);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				$data = array(
					"response" => "failed",
					"msg"      => "complain gagal di ambil",
					'csrfHash' => $this->security->get_csrf_hash(),
					"url"      => site_url("complain_masuk"),
				);
			} else {
				$data = array(
					"response" => "success",
					"msg"      => "anda berhasil ambil complain",
					'csrfHash' => $this->security->get_csrf_hash(),
					"url"      => site_url("complain_masuk"),
				);
			}
		} else {
			$data = array(
				"response" => "failed",
				"msg"      => "complain sudah di ambil oleh yang lain",
				'csrfHash' => $this->security->get_csrf_hash(),
				"url"      => site_url("complain_masuk"),
			);
		}
		return $data;
	}
}