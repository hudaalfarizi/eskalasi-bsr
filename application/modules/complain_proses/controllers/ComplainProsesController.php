<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ComplainProsesController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->permission->cek_permission('service_request_in_process')) {
			$this->load->model('ComplainProsesModel','model');
		} else {
			redirect('home');
		}
	}
	public function index()
	{
		$data = array(
            'content' => 'data_complain',
        );
        $this->load->view('layouts/template',$data);
	}
	public function data_complain_proses() {
		header('Content-Type: application/json');
		echo $this->model->get_datatables();
	}
	public function detail_complain() {
		$id     = $_GET['id'];
		$result = $this->model->detail_complain($id);
		$data   = array(
			'content' => 'detail_complain',
			'result'  => $result,
		);
		$this->load->view('layouts/template',$data);
	}
	public function update_selesai() {
		$this->form_validation->set_rules(
									'root_cause',
									'Permasalahan',
									'required',
									['required'=>'Permasalahan Harus Di Isi']
								);
		$this->form_validation->set_rules(
									'handling',
									'Penanganan',
									'required',
									['required'=>'Penanganan Harus Di Isi']
								);
		if($this->form_validation->run() === FALSE) {
			$data = array(
						'response' => 'failed',
						'msg'      => validation_errors(),
						'csrfHash' => $this->security->get_csrf_hash(),
			);
		} else {

			$ticket     = $this->input->post('ticket');
			$root_cause = $this->input->post('root_cause');
			$handling   = $this->input->post('handling');
			$images     = '';

			if(!empty($_FILES['file']['name'])) {
                $config['upload_path']   ="./assets/images/complain_upload";
                $config['allowed_types'] ='gif|jpg|png|jpeg';
                $config['encrypt_name']  = TRUE;
                
                $this->load->library('upload',$config);
                if($this->upload->do_upload('file')){
                    $data = array('upload_data' => $this->upload->data());
                    $image  = $data['upload_data']['file_name'];                   
                    $data   = $this->model->update_selesai($image,$ticket,$root_cause,$handling);
                } else {
                    $data = array(
                        'response' => 'failed',
                        'msg'      => 'gagal simpan data, silahkan hubungi IT Support',
                        'csrfHash' => $this->security->get_csrf_hash(),
                    );
                }
            } else {
				$data = $this->model->update_selesai($images,$ticket,$root_cause,$handling);
			}
		}
		echo json_encode($data);
	}
	public function pending(){
		$this->form_validation->set_rules(
									'note',
									'Note',
									'required',
									['required'=>'Alasan Pending Harus Di Isi']
								);
		if($this->form_validation->run() === FALSE) {
			$data = array(
						'response' => 'failed',
						'msg'      => validation_errors(),
						'csrfHash' => $this->security->get_csrf_hash(),
			);
		} else {
			$ticket = $this->input->post("ticket");
			$data   = $this->model->pending($ticket);
		}
		echo json_encode($data);
	}
}
