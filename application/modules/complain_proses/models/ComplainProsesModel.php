<?php

class ComplainProsesModel extends CI_Model {
    function get_datatables(){
		$site_url = site_url();
		$this->datatables->select("
					ticket.ticket_code,
					departement.departement_name,
					problem_category.problem_category_name,
                    profile.name,
					problem_category.priority,
					ticket.status,
                    CONCAT(
						'<a href=','''','$site_url','/complain_proses/detail_complain?id=',MD5(ticket.ticket_id),'''','class=','''','btn btn-sm btn-success','''','>','detail</a> ',
						'<button class=','''','btn btn-sm btn-info ini','''','data-note=','''',ticket.note,'''','>','Note</button>'
					) AS action,

		");
		$this->datatables->from("ticket");
		$this->datatables->join("profile", "profile.user_id=ticket.user_id", "left");
		$this->datatables->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left");
		$this->datatables->join("departement", "departement.departement_id=ticket.departement_id_from", "left");
		if($this->session->departement_type == 'user') {
			$this->datatables->where("ticket.departement_id_from",$this->session->departement_id);
			$this->datatables->where("(ticket.status='diproses' OR ticket.status='pending')");
		} else {
			$this->datatables->where("(ticket.departement_id_destination='".$this->session->departement_id."' AND ticket.teknisi='".$this->session->user_id."' AND ticket.status='diproses' OR ticket.status='menunggu_approve' OR ticket.status='pending')");
			$this->datatables->or_where("(`ticket.status`='pending')");
		}
		return $this->datatables->generate();
	}
	function detail_complain($id){
		$data = $this->db->select("
								ticket.ticket_id,
								ticket.ticket_code,
								ticket.departement_id_destination,
								departement.departement_name,
								problem_category.problem_category_name,
								profile.name,
								ticket.description,
								ticket.created_at,
								ticket.updated_at,
								ticket.image,
								ticket.status,
								ticket.note,
								problem_category.priority,
						 ")
						 ->from("ticket")
						 ->join("profile", "profile.user_id=ticket.user_id", "left")
						 ->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left")
						 ->join("departement", "departement.departement_id=ticket.departement_id_from", "left")
						 ->where("md5(ticket.ticket_id)",$id)
						 ->get()->row();
		return $data;
	}
	function update_selesai($image = '',$ticket,$root_cause,$handling) {
		$cek_selesai = $this->db->get_where('ticket',['ticket_id'=>$ticket,'status'=>'diproses']);
		if($cek_selesai->num_rows() > 0) {
			$assignment = array(
				"root_cause"  => $root_cause,
				"handling"    => $handling,
				"complied_by" => $this->session->user_id,
				"end_at"      => date('Y-m-d H:i:s'),
				"image"       => $image,
			);
			$created  = $cek_selesai->row()->created_at;
			$now      = date("Y-m-d");
			$trace    = $this->db->select('*')
			                     ->from('tracelog_ticket')
			                     ->where('ticket_id',$ticket)
			                     ->order_by('tracelog_ticket_id','desc')
			                     ->limit(1)
			                     ->get();
			$date1    = date('Y-m-d');
			$time1    = date('H:i');
			if(!empty($this->input->post('date_s'))) {
				$date1 = $this->input->post('date_s');
			}
			if(!empty($this->input->post('time_s'))) {
				$time1 = $this->input->post('time_s');
			}
			$tglexec   = $trace->row()->date;
			$str_now   = $date1." ".$time1.":00";
			$str_last  = $tglexec." ".$trace->row()->execute;
			$dif_now   = new DateTime($str_now);
			$dif_last  = new DateTime($str_last);
			$diff      = date_diff($dif_now,$dif_last);
			$note      = $this->input->post('note');
			$minute    = $diff->days * 24 * 60;
			$minute   += $diff->h * 60;
			$minute   += $diff->i;
			$traceid  = $trace->row()->tracelog_ticket_id;
			$mexec    = $trace->row()->execute;
			$wskrg    = date('Y-m-d H:i');
			if(strtotime($date1) < strtotime($tglexec)) {
				$data = array(
					"response" => "failed",
					"msg"      => "tanggal selesai tidak bisa lebih cepat dari pengambilan",
					'csrfHash' => $this->security->get_csrf_hash(),
				);

			} elseif (strtotime($date1) == strtotime($tglexec)) {
				if(strtotime($time1) < strtotime($mexec)) {
						$data = array(
						"response" => "failed",
						"msg"      => "jam selesai tidak bisa lebih cepat dari pengambilan",
						'csrfHash' => $this->security->get_csrf_hash(),
					);
				} else {
					$this->db->trans_start();
					$this->db->where("ticket_id",$ticket);
					$this->db->update("ticket",["status"=>"menunggu_approve"]);
					$this->db->where("ticket_id",$ticket);
					$this->db->update("activity_assignment",$assignment);
					if($now == $tglexec) {
						$this->db->where("tracelog_ticket_id",$traceid);
						$this->db->update("tracelog_ticket",["finish"=>$time1,"manhours"=>$minute]);
						$sql_total = "SELECT SUM(manhours) as total FROM tracelog_ticket WHERE ticket_id='".$ticket."'";
						$mtotal    = $this->db->query($sql_total)->row();
						$tracelog = array(
							"ticket_id" => $ticket,
							"manhours"  => "total : " . $mtotal->total . " menit",

						);
						$this->db->insert("tracelog_ticket",$tracelog);
					} else {
						$tracelog = array(
							"ticket_id" => $ticket,
							"pic"       => $this->session->user_id,
							"date"      => $date1,
							"finish"    => $time1,
							"manhours"  => $minute,
						);
						$this->db->insert("tracelog_ticket",$tracelog);
						$sql_total = "SELECT SUM(manhours) as total FROM tracelog_ticket WHERE ticket_id='".$ticket."'";
						$mtotal    = $this->db->query($sql_total)->row();
						$tracelog2 = array(
							"ticket_id" => $ticket,
							"manhours"  => "total : " . $mtotal->total . " menit",

						);
						$this->db->insert("tracelog_ticket",$tracelog2);
					}
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE)
					{
						$data = array(
							"response" => "failed",
							"msg"      => "complain gagal di selesaikan",
							'csrfHash' => $this->security->get_csrf_hash(),
							"url"      => site_url("complain_masuk"),
						);
					} else {
						$data = array(
							"response" => "success",
							"msg"      => "anda berhasil selesaikan complain",
							'csrfHash' => $this->security->get_csrf_hash(),
							"url"      => site_url("complain_masuk"),
						);
					}
				}
			} else {
				$this->db->trans_start();
					$this->db->where("ticket_id",$ticket);
					$this->db->update("ticket",["status"=>"menunggu_approve"]);
					$this->db->where("ticket_id",$ticket);
					$this->db->update("activity_assignment",$assignment);
					if($now == $tglexec) {
						$this->db->where("tracelog_ticket_id",$traceid);
						$this->db->update("tracelog_ticket",["finish"=>$time1,"manhours"=>$minute]);
						$sql_total = "SELECT SUM(manhours) as total FROM tracelog_ticket WHERE ticket_id='".$ticket."'";
						$mtotal    = $this->db->query($sql_total)->row();
						$tracelog = array(
							"ticket_id" => $ticket,
							"manhours"  => "total : " . $mtotal->total . " menit",

						);
						$this->db->insert("tracelog_ticket",$tracelog);
					} else {
						$tracelog = array(
							"ticket_id" => $ticket,
							"pic"       => $this->session->user_id,
							"date"      => $date1,
							"finish"    => $time1,
							"manhours"  => $minute,
						);
						$this->db->insert("tracelog_ticket",$tracelog);
						$sql_total = "SELECT SUM(manhours) as total FROM tracelog_ticket WHERE ticket_id='".$ticket."'";
						$mtotal    = $this->db->query($sql_total)->row();
						$tracelog2 = array(
							"ticket_id" => $ticket,
							"manhours"  => "total : " . $mtotal->total . " menit",

						);
						$this->db->insert("tracelog_ticket",$tracelog2);
					}
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE)
					{
						$data = array(
							"response" => "failed",
							"msg"      => "complain gagal di selesaikan",
							'csrfHash' => $this->security->get_csrf_hash(),
							"url"      => site_url("complain_masuk"),
						);
					} else {
						$data = array(
							"response" => "success",
							"msg"      => "anda berhasil selesaikan complain",
							'csrfHash' => $this->security->get_csrf_hash(),
							"url"      => site_url("complain_masuk"),
						);
					}
			}
		} else {
			$data = array(
				"response" => "failed",
				"msg"      => "complain sudah di selesaikan oleh yang lain",
				'csrfHash' => $this->security->get_csrf_hash(),
				"url"      => site_url("complain_masuk"),
			);
		}
		return $data;
	}

	function pending($ticket) {
		$cek_ticket = $this->db->get_where("ticket",["ticket_id"=>$ticket,"status"=>"diproses"]);
		if($cek_ticket->num_rows() === 1) {
			$now      = date("Y-m-d");
			$trace    = $this->db->select('*')
										 ->from('tracelog_ticket')
										 ->where('ticket_id',$ticket)
										 ->order_by('tracelog_ticket_id','desc')
										 ->limit(1)
										 ->get();
			$tglexec  = $trace->row()->date;
			$execute  = strtotime($trace->row()->execute);
			$idticket = $trace->row()->tracelog_ticket_id;
			$date1    = date('Y-m-d');
			$time1    = date('H:i');
			if(!empty($this->input->post('date'))) {
				$date1 = $this->input->post('date');
			}
			if(!empty($this->input->post('time'))) {
				$time1 = $this->input->post('time');
			}
			$str_now   = $date1." ".$time1.":00";
			$str_last  = $tglexec." ".$trace->row()->execute;
			$dif_now   = new DateTime($str_now);
			$dif_last  = new DateTime($str_last);
			$diff      = date_diff($dif_now,$dif_last);
			$note      = $this->input->post('note');
			$minute    = $diff->days * 24 * 60;
			$minute   += $diff->h * 60;
			$minute   += $diff->i;
			if(strtotime($date1) < strtotime($tglexec)) {
				$data = array(
					"response" => "failed",
					"msg"      => "tanggal pending tidak bisa lebih cepat dari pengambilan",
					'csrfHash' => $this->security->get_csrf_hash(),
				);

			} elseif (strtotime($date1) == strtotime($tglexec)) {
				if(strtotime($time1) < $execute) {
						$data = array(
						"response" => "failed",
						"msg"      => "jam pending tidak bisa lebih cepat dari pengambilan",
						'csrfHash' => $this->security->get_csrf_hash(),
					);
				} else {
					$this->db->trans_start();
					$this->db->where("ticket_id",$ticket);
					$this->db->update("ticket",["status"=>"pending","note"=>$note]);
					if(strtotime($date1) == strtotime($tglexec)) {
						$this->db->where("tracelog_ticket_id",$idticket);
						$this->db->update("tracelog_ticket",["date"=>$date1,"pending"=>$time1,"manhours"=>$minute,"note"=>$note]);
					} else {
						$tracelog = array(
							"ticket_id" => $ticket,
							"pic"       => $this->session->user_id,
							"date"      => $date1,
							"pending"   => $time1,
							"manhours"  => $minute,
							"note"      => $note,
						);
						$this->db->insert("tracelog_ticket",$tracelog);
					}
					$this->db->trans_complete();
					if ($this->db->trans_status() === FALSE)
					{
						$data = array(
							"response" => "failed",
							"msg"      => "complain gagal di pending",
							'csrfHash' => $this->security->get_csrf_hash(),
							"url"      => site_url("complain_proses"),
						);
					} else {
						$data = array(
							"response" => "success",
							"msg"      => "anda berhasil pending complain",
							'csrfHash' => $this->security->get_csrf_hash(),
							"url"      => site_url("complain_proses"),
						);
					}
				}
			} else {
				$this->db->trans_start();
				$this->db->where("ticket_id",$ticket);
				$this->db->update("ticket",["status"=>"pending"]);
				if(strtotime($date1) == strtotime($tglexec)) {
					$this->db->where("tracelog_ticket_id",$idticket);
					$this->db->update("tracelog_ticket",["date"=>$date1,"pending"=>$time1,"manhours"=>$minute,"note"=>$note]);
				} else {
					$tracelog = array(
						"ticket_id" => $ticket,
						"pic"       => $this->session->user_id,
						"date"      => $date1,
						"pending"   => $time1,
						"manhours"  => $minute,
						"note"      => $note,
					);
					$this->db->insert("tracelog_ticket",$tracelog);
				}
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					$data = array(
						"response" => "failed",
						"msg"      => "complain gagal di pending",
						'csrfHash' => $this->security->get_csrf_hash(),
						"url"      => site_url("complain_proses"),
					);
				} else {
					$data = array(
						"response" => "success",
						"msg"      => "anda berhasil pending complain",
						'csrfHash' => $this->security->get_csrf_hash(),
						"url"      => site_url("complain_proses"),
					);
				}
			}
			
		} else {
			$data = array(
				"response" => "failed",
				"msg"      => "complain sudah di response tim lain",
				'csrfHash' => $this->security->get_csrf_hash(),
				"url"      => site_url("complain_proses"),
			);
		}
		return $data;
	}
}