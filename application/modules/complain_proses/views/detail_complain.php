<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.timepicker.css');?>">
<div class="col-12 col-sm-8 col-md-9">
   <div class="card">
      <div class="card-body">
         <h5><b>Detail Complain</b></h5>
         <hr>
         <br>
         <table class="table table-striped table-hover">
        <input type="hidden" id="ticket" value="<?php echo $result->ticket_id ?>">
            <tr>
               <td class="title-td">kode Tiket</td>
               <td>:</td>
               <td><?php echo htmlspecialchars($result->ticket_code) ;?></td>
            </tr>
            <tr>
               <td class="title-td">Departement</td>
               <td>:</td>
               <td><?php echo htmlspecialchars($result->departement_name) ;?></td>
            </tr>
            <tr>
               <td class="title-td">Kategori Masalah</td>
               <td>:</td>
               <td><?php echo htmlspecialchars($result->problem_category_name) ;?></td>
            </tr>
            <tr>
               <td class="title-td">Nama Pelapor</td>
               <td>:</td>
               <td><?php echo htmlspecialchars($result->name) ;?></td>
            </tr>
            <tr>
               <td class="title-td">Detail Masalah</td>
               <td>:</td>
               <td><?php echo htmlspecialchars($result->description) ;?></td>
            </tr>
            <tr>
               <td class="title-td">Waktu Komplain</td>
               <td>:</td>
               <td><?php echo htmlspecialchars($result->created_at) ;?></td>
            </tr>
            <?php if($result->image != '') { ?>
            <tr>
               <td class="title-td">Gambar</td>
               <td>:</td>
               <td><a target="_blank" href="<?php echo base_url('assets/images/complain_upload/'.$result->image);?>">images</a></td>
            </tr>
            <?php } ?>
            <tr>
               <td class="title-td">Prioritas Masalah</td>
               <td>:</td>
               <td><?php echo htmlspecialchars($result->priority) ;?></td>
            </tr>
            <?php if($result->status == 'pending') { ?>
            <tr>
               <td class="title-td">Note Pending</td>
               <td>:</td>
               <td><?php echo htmlspecialchars($result->note) ;?></td>
            </tr>
            <?php } ?>
         </table>
         <hr>
         <?php if($result->status == 'pending') { ?>
            <button class="btn btn-success" id="ambil">ambil</button>
         <?php } ?>
         
         <div class="information"></div>
         <?php if($result->status==='diproses' && $this->permission->cek_permission('done')) { ?>
         <form class="form-file" data-uri="<?php echo site_url("complain_update_selesai");?>">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf">
            <input type="hidden" id="ticket" name="ticket" value="<?php echo $result->ticket_id ?>">
            <b class="note_selesai">Permasalahan</b>
            <br class="note_selesai">
            <textarea name="root_cause" id="root_cause" cols="30" class="form-control note_selesai"></textarea>
            <br class="note_selesai">
            <b class="note_selesai">Penanganan</b>
            <br class="note_selesai">
            <textarea name="handling" id="handling" cols="30" class="form-control note_selesai"></textarea>
            <br class="note_selesai">
            <b class="note_selesai">Image</b>
            <input class="note_selesai form-control" type="file" name="file" accept="image/x-png,image/gif,image/jpeg" />
            <br class="note_selesai">
            <b class="note_selesai">Tanggal</b>
            <br class="note_selesai">
            <input type="text" name="date_s" id="date_s" class="note_selesai form-control" autocomplete="off">
            <br class="note_selesai">
            <b class="note_selesai">Jam</b>
            <br class="note_selesai">
            <input type="text" name="time_s" id="time_s" class="note_selesai form-control" autocomplete="off">
            <hr class="note_selesai">
            <input type="submit" name="submit" value="Selesai" class="btn btn-sm btn-success note_selesai" style="float:left;">
         </form>
         <?php } ?>
         <?php if($result->status==='diproses' && $this->permission->cek_permission('pending')) { ?>
         <button id="pending" class="btn btn-sm btn-danger">pending</button>
         <button id="batal_pending" style="display: none;" class="btn btn-sm btn-danger note_pending">batal pending</button>
         <br class="note_pending">
         <br class="note_pending">
         <br class="note_pending">
         <form class="form-file" data-uri="<?php echo site_url("complain_pending");?>">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf">
            <b class="note_pending">Tanggal</b>
            <br class="note_pending">
            <input type="text" name="date" id="date" class="note_pending form-control" autocomplete="off">
            <br class="note_pending">
            <b class="note_pending">Jam</b>
            <br class="note_pending">
            <input type="text" name="time" id="time" class="note_pending form-control" autocomplete="off">
            <br class="note_pending">
            <b class="note_pending">Note</b>
            <br class="note_pending">
            <input type="hidden" id="ticket" name="ticket" value="<?php echo $result->ticket_id ?>">
            <textarea class="form-control note_pending" name="note" id="note"></textarea>
            <br class="note_pending">
            <br class="note_pending">
            <input type="submit" name="submit" value="apply" class="btn btn-sm btn-warning note_pending" id="apply">
         </form>
         <?php } ?>
         <?php if($result->status==='menunggu_approve' && $this->permission->cek_permission('complain_approve')) { ?>
         <form class="form-file" data-uri="<?php echo site_url("complain_update_approve");?>">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf">
            <input type="hidden" id="ticket" name="ticket" value="<?php echo $result->ticket_id ?>">
            <input type="submit" name="submit" value="Selesai" class="btn btn-sm btn-success">
         </form>
         <?php } ?>
      </div>
   </div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery.timepicker.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
<script>
   $(".note_pending").hide();
   $("#pending").on("click",function(){
       $(".note_selesai").hide();
       $("#pending").hide();
       $(".note_pending").show();
       $("#note").focus();
   })
   $("#batal_pending").on("click",function(){
       $(".note_pending").hide();
       $(".note_selesai").show();
       $("#note").focus();
   })
   $("#date").datepicker({
        dateFormat: 'yy-mm-dd'
   });
   $('#time').timepicker({
       timeFormat: 'HH:mm',
       interval:5,
   });
   $("#date_s").datepicker({
        dateFormat: 'yy-mm-dd'
   });
   $('#time_s').timepicker({
       timeFormat: 'HH:mm',
       interval:5,
   });
    $("#ambil").on("click", function(){
        var ticket = $("#ticket").val();
        var csrfName       = $("#csrf").attr('name');
        var csrfHash       = $("#csrf").val();
        var myJson         = {ticket:ticket,[csrfName]:csrfHash};
        $.ajax({
            url      : "<?php echo site_url('ambil_complain');?>",
            data     : myJson,
            type     : "POST",
            dataType : "JSON",

            success : function(data) {
                if (data.response == 'success') {
                    Swal.fire({
                        icon: 'success',
                        title: 'Selamat !',
                        text: data.msg,
                        type: "success"
                    }).then(function () {
                        window.location = data.url;
                    });
                } else if(data.response == 'failed') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Ada Kesalahan !',
                        text: 'terjadi kesalahan',
                        type: "error"
                    });
                    $("#csrf").val(data.csrfHash);
                }
            }
        });
    });
</script>