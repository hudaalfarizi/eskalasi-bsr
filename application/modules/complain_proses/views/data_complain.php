<div class="col-12 col-sm-8 col-md-9">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Service Request in Process</b></h5>
			<hr>	
			<br>
			<div class="table-responsive">
				<table id="table-all" data-source="<?php echo site_url("get_complain_proses");?>">
					<thead>
						<th>Tiket</th>
						<th>Departement Pelapor</th>
						<th>Kategori Masalah</th>
						<th>Nama Pelapor</th>
						<th>Prioritas</th>
						<th>Status</th>
						<th>Action</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Note</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dt.css')?>">
<script src="<?php echo base_url('assets/js/dt.js');?>"></script>
<script>
$( document ).ready(function() {
    data_table();
});
$(document).on('click', '.ini', function (e) {
    var note  = $(this).attr('data-note');
    if(note == "") {
      note = "Tidak ada Note";
    }
    $(".modal").modal("show");
    $(".modal-body").text(note);
 });
function data_table()
{
	var url     = $('#table-all').attr('data-source');
	var token   = "<?php echo $this->security->get_csrf_hash(); ?>";
	var csrfname= "<?php echo $this->security->get_csrf_token_name(); ?>";

	$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
	{
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};

	var table = $("#table-all").dataTable({
		initComplete: function() {
			var api = this.api();
			$('#mytable_filter input')
				.off('.DT')
				.on('input.DT', function() {
					api.search(this.value).draw();
			});
		},
			oLanguage: {
			sProcessing: "loading..."
		},
			processing: true,
			serverSide: true,
			ajax: {
				"url": url,
				"type": "POST",
				"data":{[csrfname]:token}
				},
				  columns: [
					  {"data": "ticket_code"},
					  {"data": "departement_name"},
					  {"data": "problem_category_name"},
					  {"data": "name"},
					  {"data": "priority"},
					  {"data": "status"},
					  {"data": "action"}
				],
			columnDefs: [{
					"targets" : [6],
					"orderable": false,
					"searchable": false,
				  }],
			order: [],
		rowCallback: function(row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			$('td:eq(0)', row).html();
		}

	});
}
</script>
