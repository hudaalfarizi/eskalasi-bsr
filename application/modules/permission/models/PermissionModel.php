<?php

class PermissionModel extends CI_Model
{
    function get_datatables()
    {
        $site_url = site_url();
        $tipe     = (int)$this->input->post('tipe');
        $iddata   = (int)$this->input->post('iddata');
        if ($tipe === 1) {
            $this->datatables->select("
                        m.module_name,
                        CONCAT(
                            '<a data-uri=','''','$site_url','/permission/delete_permission','''',' data-source=','''',MD5(p.permission_id),'''',' class=','''','btn btn-sm btn-danger delete_permission f-white','''','>delete</a>'
                        ) AS action,

            ");
            $this->datatables->from("permission p");
            $this->datatables->join("module m", "m.module_id=p.module_id", "left");
            $this->datatables->where("p.position_id", $iddata);
            return $this->datatables->generate();
        } elseif ($tipe === 2) {
            $this->datatables->select("
                        m.module_name,
                        CONCAT(
                            '<a data-uri=','''','$site_url','permission/delete_permission','''',' data-source=','''',MD5(p.permission_id),'''',' class=','''','btn btn-sm btn-danger delete_permission f-white','''','>delete</a>'
                        ) AS action,

            ");
            $this->datatables->from("permission p");
            $this->datatables->join("module m", "m.module_id=p.module_id", "left");
            $this->datatables->where("p.user_id", $iddata);
            return $this->datatables->generate();
        }
    }
    function pilih_berdasarkan()
    {
        $pilih = (int)$this->input->post('id');
        if ($pilih === 1) {
            $query = $this->db->select("
                                    p.position_id as data_id,
                                    p.position_name as data_name,
                                    d.departement_name
                                ")
                ->from('position p')
                ->join('departement d', 'd.departement_id=p.departement_id', 'left')
                ->get()->result();
            $data = array(
                'response' => 'success',
                'msg'      => $query,
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        } elseif ($pilih === 2) {
            $query = $this->db->select("
                                    u.user_id as data_id,
                                    u.username as data_name,
                                ")
                ->from('user u')
                ->get()->result();
            $data = array(
                'response' => 'success',
                'msg'      => $query,
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        }
        return $data;
    }
    function get_module()
    {
        return $this->db->get('module')->result();
    }
    function select_module()
    {
        $tipe     = (int)$this->input->post('tipe');
        $iddata   = (int)$this->input->post('iddata');
        $datamodule = array();
        if ($tipe === 1) {
            $cek = $this->db->select('module_id')
                ->from('permission')
                ->where('position_id', $iddata)
                ->get()->result_array();
            foreach ($cek as $row) {
                array_push($datamodule, $row['module_id']);
            }
        } elseif ($tipe === 2) {
            $cek = $this->db->select('module_id')
                ->from('permission')
                ->where('user_id', $iddata)
                ->get()->result_array();
            foreach ($cek as $row) {
                array_push($datamodule, $row['module_id']);
            }
        }
        if (count($datamodule) == 0) {
            $datamodule = 0;
        }
        $get_module = $this->db->select('module_id,module_name')
            ->from('module')
            ->where_not_in('module_id', $datamodule)
            ->get()->result();
        $data = array(
            'response' => 'success',
            'msg'      => $get_module,
            'csrfHash' => $this->security->get_csrf_hash(),
        );
        return $data;
    }
    public function update_permission()
    {
        $tipe     = (int)$this->input->post('tipe');
        $iddata   = (int)$this->input->post('iddata');
        $idmodule = $this->input->post('idmodul');
        if ($tipe === 1) {
            $cek = $this->db->select('module_id')
                ->from('permission')
                ->where('position_id', $iddata)
                ->where('module_id', $idmodule)
                ->get()->num_rows();
            if ($cek > 0) {
                $data = array(
                    'response' => 'failed',
                    'msg'      => 'data sudah ada',
                    'csrfHash' => $this->security->get_csrf_hash(),
                );
            } else {
                $insert = array(
                    'position_id' => $iddata,
                    'module_id'   => $idmodule,
                );
                $simpan = $this->db->insert('permission', $insert);
                if ($simpan) {
                    $data = array(
                        'response' => 'success',
                        'msg'      => 'berhasil simpan data',
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'url'      => site_url('permission'),
                    );
                } else {
                    $data = array(
                        'response' => 'failed',
                        'msg'      => 'ada kesalahan saat simpan data silahkan hubungi it',
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'url'      => site_url('permission'),
                    );
                }
            }
        } elseif ($tipe === 2) {
            $cek = $this->db->select('module_id')
                ->from('permission')
                ->where('user_id', $iddata)
                ->where('module_id', $idmodule)
                ->get()->num_rows();
            if ($cek > 0) {
                $data = array(
                    'response' => 'failed',
                    'msg'      => 'data sudah ada',
                    'csrfHash' => $this->security->get_csrf_hash(),
                );
            } else {
                $insert = array(
                    'user_id'     => $iddata,
                    'module_id'   => $idmodule,
                );
                $simpan = $this->db->insert('permission', $insert);
                if ($simpan) {
                    $data = array(
                        'response' => 'success',
                        'msg'      => 'berhasil simpan data',
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'url'      => site_url('permission'),
                    );
                } else {
                    $data = array(
                        'response' => 'failed',
                        'msg'      => 'ada kesalahan saat simpan data silahkan hubungi it',
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'url'      => site_url('permission'),
                    );
                }
            }
        }
        return $data;
    }
    function delete_permission()
    {
        $id = $this->input->post('id');
        $this->db->where('MD5(permission_id)', $id);
        $hapus = $this->db->delete('permission');
        if ($hapus) {
            $data = array(
                'response' => 'success',
                'msg'      => 'berhasil hapus data',
                'csrfHash' => $this->security->get_csrf_hash(),
                'url'      => site_url('permission'),
            );
        } else {
            $data = array(
                'response' => 'failed',
                'msg'      => 'ada kesalahan saat hapus data silahkan hubungi it',
                'csrfHash' => $this->security->get_csrf_hash(),
                'url'      => site_url('permission'),
            );
        }
        return $data;
    }
}
