<?php defined('BASEPATH') or exit('No direct Script Access Allowed');

class PermissionController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->permission->cek_permission('permission')) {
            $this->load->model('PermissionModel', 'model');
        } else {
            redirect('home');
        }
    }
    public function index()
    {
        $data = array(
            'content'     => 'permission',
            'module' => $this->model->get_module(),
        );
        $this->load->view('layouts/template', $data);
    }
    public function pilih_berdasarkan()
    {
        $data = $this->model->pilih_berdasarkan();
        echo json_encode($data);
    }
    public function data_permission()
    {
        header('Content-Type: application/json');
        echo $this->model->get_datatables();
    }
    public function select_module()
    {
        $data = $this->model->select_module();
        echo json_encode($data);
    }
    public function update_permission()
    {
        $data = $this->model->update_permission();
        echo json_encode($data);
    }
    public function delete_permission()
    {
        $data = $this->model->delete_permission();
        echo json_encode($data);
    }
}
