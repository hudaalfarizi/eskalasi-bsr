<div class="col-12 col-sm-8 col-md-9">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Service Request Completed</b></h5>
			<hr>	
			<br>
			<div class="table-responsive">
				<table id="table-all" data-source="<?php echo site_url("get_complain_selesai");?>">
					<thead>
						<th>Tiket</th>
						<th>Departement Pelapor</th>
						<th>Kategori Masalah</th>
						<th>Nama Pelapor</th>
						<th>Prioritas</th>
						<th style="<?($this->session->departement_type == 'maintenance') ? 'display:none !important;' : ''; ?>">Action</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dt.css')?>">
<script src="<?php echo base_url('assets/js/dt.js');?>"></script>
<script>
$( document ).ready(function() {
    data_table();
});
function data_table()
{
	var url     = $('#table-all').attr('data-source');
	var token   = "<?php echo $this->security->get_csrf_hash(); ?>";
	var csrfname= "<?php echo $this->security->get_csrf_token_name(); ?>";

	$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
	{
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};

	var table = $("#table-all").dataTable({
		initComplete: function() {
			var api = this.api();
			$('#mytable_filter input')
				.off('.DT')
				.on('input.DT', function() {
					api.search(this.value).draw();
			});
		},
			oLanguage: {
			sProcessing: "loading..."
		},
			processing: true,
			serverSide: true,
			ajax: {
				"url": url,
				"type": "POST",
				"data":{[csrfname]:token}
				},
				  columns: [
					  {"data": "ticket_code"},
					  {"data": "departement_name"},
					  {"data": "problem_category_name"},
					  {"data": "name"},
					  {"data": "priority"},
					  {"data": "action"}
				],
			columnDefs: [{
					"targets" : [5],
					"orderable": false,
					"searchable": false,
				  }],
			order: [],
		rowCallback: function(row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			$('td:eq(0)', row).html();
		}

	});
}
</script>
