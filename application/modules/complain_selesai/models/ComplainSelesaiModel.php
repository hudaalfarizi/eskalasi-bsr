<?php

class ComplainSelesaiModel extends CI_Model {
    function get_datatables(){
		$site_url = site_url();
		$this->datatables->select("
					ticket.ticket_code,
					departement.departement_name,
					problem_category.problem_category_name,
                    profile.name,
					problem_category.priority,
					CONCAT(
						'<a href=','''','$site_url','/complain_selesai/detail_complain?id=',MD5(ticket.ticket_id),'''','class=','''','btn btn-sm btn-success','''','>','lihat</a> '
					) AS action,

		");
		$this->datatables->from("ticket");
		$this->datatables->join("profile", "profile.user_id=ticket.user_id", "left");
		$this->datatables->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left");
		$this->datatables->join("departement", "departement.departement_id=ticket.departement_id_from", "left");
		if($this->session->departement_type == 'user') {
			$this->datatables->where("ticket.departement_id_from",$this->session->departement_id);
		} else {
			$this->datatables->where("ticket.departement_id_destination",$this->session->departement_id);
		}
		$this->datatables->where("ticket.status","selesai");
		return $this->datatables->generate();
	}
	function detail_complain($id){
		$data = $this->db->select("
								ticket.ticket_id,
								ticket.ticket_code,
								ticket.departement_id_from,
								departement.departement_name,
								problem_category.problem_category_name,
								profile.name,
								ticket.description,
								ticket.created_at,
								ticket.updated_at,
								ticket.image,
								problem_category.priority,
								act.root_cause,
								act.handling
						 ")
						 ->from("ticket")
						 ->join("activity_assignment act", "act.ticket_id=ticket.ticket_id", "left")
						 ->join("profile", "profile.user_id=ticket.user_id", "left")
						 ->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left")
						 ->join("departement", "departement.departement_id=ticket.departement_id_from", "left")
						 ->where("md5(ticket.ticket_id)",$id)
						 ->get()->row();
		return $data;
	}
	function tutup_complain($ticket) {
		$cek = $this->db->get_where("ticket",["ticket_id"=>$ticket,"status"=>"selesai"])->num_rows();
		if($cek > 0) {
			$this->db->where("ticket_id",$ticket);
			$update = $this->db->update("ticket",["status"=>"closed","updated_at"=>date('y-m-d h:i:s')]);
			if($update) {
				$data = array(
					"response" => "success",
					"msg"      => "berhasil tutup complain",
					"url"      => site_url("complain_selesai"),
				);
			} else {
				$data = array(
					"response" => "failed",
					"msg"      => "gagal tutup complain",
					"url"      => site_url("complain_selesai"),
				);
			}
		} else {
			$data = array(
				"response" => "failed",
				"msg"      => "sudah tutup complain",
				"url"      => site_url("complain_selesai"),
			);
		}
		return $data;
	}
}