<?php defined('BASEPATH') or exit ('No direct Access Allowed');

class AuthorizationController extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('AuthorizationModel','model');
    }
    public function index() {
        if($this->session->is_login == TRUE) {
            redirect('home');
        }
        $data = array(
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
        );
        $this->load->view('authorization',$data);
    }
    public function cek_validation() {
        $data = $this->model->cek_validation();
        echo json_encode($data);
    }
    public function logout() {
        foreach ($_SESSION as $key => $value) {
			unset($_SESSION[$key]);
		}
		$this->session->sess_destroy();
		redirect('/');
    }
}
