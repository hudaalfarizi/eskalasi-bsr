<?php

class AuthorizationModel extends CI_Model {

    public function cek_validation() {
        $username = $this->input->post('username');
        $password = sha1($this->input->post('password'));

        $cek = $this->db->get_where('user',['username'=>$username,'password'=>$password])->num_rows();
        if($cek > 0) {
            $get_information = $this->db->select('
                                            u.user_id,
                                            u.username,
                                            u.idcard,
                                            p.position_name,
                                            p.position_id,
                                            d.departement_id,
                                            d.departement_name,
                                            d.departement_type,
                                        ')
                                        ->from('user u')
                                        ->join('position p','p.position_id=u.position_id')
                                        ->join('departement d','d.departement_id=p.departement_id')
                                        ->where(['username'=>$username,'password'=>$password])
                                        ->get()->row();
            $sesi = array(
                'is_login'         => true,
                'user_id'          => $get_information->user_id,
                'username'         => $get_information->username,
                'idcard'           => $get_information->idcard,
                'position_id'      => $get_information->position_id,
                'position_name'    => $get_information->position_name,
                'departement_id'   => $get_information->departement_id,
                'departement_type' => $get_information->departement_type,
            );
            $this->session->set_userdata($sesi);
            $data = array(
                'response' => 'success',
                'msg'      => 'anda berhasil login',
                'csrfHash' => $this->security->get_csrf_hash(),
                'url'      => site_url('home'),
            );
        } else {
            $data = array(
                'response' => 'failed',
                'msg'      => 'user tidak di temukan',
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        }
        return $data;
    }

}