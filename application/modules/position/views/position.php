<div class="col-12 col-sm-6">
	<div class="information"></div>
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Position</b></h5>
			<hr>	
			<br>
			<div class="table-responsive">
				<table id="table-all" data-source="<?php echo site_url("data_position");?>">
					<thead>
						<th>Position Name</th>
						<th>Departement Name</th>
						<th>Action</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="col-12 col-sm-3">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Add Position</b></h5>
			<hr>	
			<br>
			<form class="form-post" data-uri="<?php echo site_url('add_position');?>">
				
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf">
				<input type="text" name="position_name" class="form-control mb-3" placeholder="Enter Position">
				<select name="departement" id="type" class="form-control">
					<option value=""></option>
					<?php 
						foreach($departement as $row) {
							echo "<option value='" .$row->departement_id. "'> " .$row->departement_name. " </option>";
						}
					?>
				</select>
				<br>
				<input type="submit" name="submit" value='simpan' class="form-control">
			</form>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Position</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<form class="form-post" data-uri="<?php echo site_url('update_position') ?>">
        <p>
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" class="csrf">	
			<input type="hidden" name="position_id" id="position_id">
			<input type="text" name="position_name" id="position_name" class="form-control mb-3">
			<select name="departement" id="departement" class="form-control">
					<option value="" id="departement_edit"></option>
					<?php 
						foreach($departement as $row) {
							echo "<option value='" .$row->departement_id. "'> " .$row->departement_name. " </option>";
						}
					?>
			</select>
		</p>
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
	  </form>
</div>

<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dt.css')?>">
<script src="<?php echo base_url('assets/js/dt.js');?>"></script>
<script>
$( document ).ready(function() {
    data_table();
});
function data_table()
{
	var url     = $('#table-all').attr('data-source');
	var token   = "<?php echo $this->security->get_csrf_hash(); ?>";
	var csrfname= "<?php echo $this->security->get_csrf_token_name(); ?>";

	$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
	{
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};

	var table = $("#table-all").dataTable({
		initComplete: function() {
			var api = this.api();
			$('#mytable_filter input')
				.off('.DT')
				.on('input.DT', function() {
					api.search(this.value).draw();
			});
		},
			oLanguage: {
			sProcessing: "loading..."
		},
			processing: true,
			serverSide: true,
			ajax: {
				"url": url,
				"type": "POST",
				"data":{[csrfname]:token}
				},
				  columns: [
					  {"data": "position_name"},
					  {"data": "departement_name"},
					  {"data": "action"}
				],
			columnDefs: [{
					"targets" : [2],
					"orderable": false,
					"searchable": false,
				  }],
			order: [],
		rowCallback: function(row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			$('td:eq(0)', row).html();
		}

	});
}
</script>
