<?php defined('BASEPATH') or exit ('No direct Script Access Allowed');

class PositionController extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if($this->permission->cek_permission('position')) {
			$this->load->model('PositionModel','model');
		} else {
			redirect('home');
		}
    }
    public function index() {
        $data = array(
            'content'     => 'position',
            'departement' => $this->model->get_departement(),
        );
        $this->load->view('layouts/template',$data);
    }
    public function data_position() {
        header('Content-Type: application/json');
		echo $this->model->get_datatables();
    }
    public function add_position() {
		$this->form_validation->set_rules(
				'position_name',
				'Nama Position',
				'required',
				array('required'=>'Nama Posisi wajib di isi')
		);
		$this->form_validation->set_rules(
				'departement',
				'Nama Departement',
				'required',
				array('required'=>'departement wajib di isi')
		);
		if($this->form_validation->run() === FALSE) {
			$data = array(
				'response' => 'failed',
				'msg'      => validation_errors(),
				'csrfHash' => $this->security->get_csrf_hash(),
			);
		} else {
			$position    = $this->input->post('position_name');
			$departement = $this->input->post('departement');
			$data = $this->model->simpan($position,$departement);
		}
		echo json_encode($data);
    }
    public function delete_position() {
		$data = $this->model->hapus();
		echo json_encode($data);
	}
	public function edit_position() {
		$data = $this->model->edit_position();
		echo json_encode($data);
	}
	public function update_position() {
		$this->form_validation->set_rules(
			'position_name',
			'Nama Position',
			'required',
			array('required'=>'Nama Posisi wajib di isi')
		);
		$this->form_validation->set_rules(
				'departement',
				'Nama Departement',
				'required',
				array('required'=>'departement wajib di isi')
		);
		if($this->form_validation->run() === FALSE) {
			$data = array(
				'response' => 'failed',
				'msg'      => validation_errors(),
				'csrfHash' => $this->security->get_csrf_hash(),
			);
		} else {
			$id          = $this->input->post('position_id');
			$position    = $this->input->post('position_name');
			$departement = $this->input->post('departement');
			$data = $this->model->update_position($id,$position,$departement);
		}
		echo json_encode($data);
	}
}