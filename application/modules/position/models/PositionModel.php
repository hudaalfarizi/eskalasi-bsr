<?php 

class PositionModel extends CI_Model {
    function get_datatables(){
		$site_url = site_url();
		$this->datatables->select("
                    p.position_name,
                    d.departement_name,
                    CONCAT(
                        '<a data-uri=','''','$site_url','/position/edit_position','''',' data-source=','''',MD5(position_id),'''',' class=','''','btn btn-sm btn-warning edit','''','>edit</a> ',
						'<a data-uri=','''','$site_url','/position/delete_position','''',' data-source=','''',MD5(position_id),'''',' class=','''','btn btn-sm btn-danger delete f-white','''','>delete</a>'
					) AS action,

		");
        $this->datatables->from("position p");
        $this->datatables->join("departement d","d.departement_id=p.departement_id","left");
		return $this->datatables->generate();
    }
    function get_departement() {
        return $this->db->get('departement')->result();
    }
    function simpan($position,$departement) {
        $cek = $this->db->get_where("position",["position_name"=>$position,"departement_id"=>$departement])->num_rows();
        if($cek > 0) {
            $data = array(
                'response' => 'failed',
                'msg'      => 'posisi ini sudah pernah di tambahkan sebelum nya',
                'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('position'), 
            );
        } else {
            $insert = array(
                'position_name' => $position,
                'departement_id'=> $departement,
            );
            $stmt = $this->db->insert('position',$insert);
            if($stmt) {
                $data = array(
                    'response' => 'success',
                    'msg'      => 'berhasil simpan data',
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'url'      => site_url('position'),
                );
            } else {
                $data = array(
                    'response' => 'failed',
                    'msg'      => 'ada kesalahan saat simpan data silahkan hubungi it',
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'url'      => site_url('position'),
                );
            }
        }
		return $data;
    }
    public function hapus() {
		$id = $this->input->post('id');
		$this->db->where('MD5(position_id)',$id);
		$hapus = $this->db->delete('position');
		if($hapus) {
			$data = array(
				'response' => 'success',
				'msg'      => 'berhasil hapus data',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('position'),
			);
		} else {
			$data = array(
				'response' => 'failed',
				'msg'      => 'ada kesalahan saat hapus data silahkan hubungi it',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('position'),
			);
		}
		return $data;
    }
    public function edit_position() {
        $id    = $this->input->post('id');
        $query = $this->db->select('
                                p.position_id,
                                p.position_name,
                                d.departement_name,
                                d.departement_id
                            ')
                           ->from('position p')
                           ->join('departement d','d.departement_id=p.departement_id')
                           ->where('MD5(position_id)',$id)
                           ->get()->row();
        $data = array(
            'reponse'  => 'success',
            'msg'      => $query,
            'csrfHash' => $this->security->get_csrf_hash(),
        );
        return $data;
    }
    public function update_position($id,$position,$departement) {
        $upd = array(
            'position_name'  => $position,
            'departement_id' => $departement,
        );
        $this->db->where('position_id',$id);
        $update=$this->db->update('position',$upd);
        if($update) {
			$data = array(
				'response' => 'success',
				'msg'      => 'berhasil update data',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('position'),
			);
		} else {
			$data = array(
				'response' => 'failed',
				'msg'      => 'ada kesalahan saat update data silahkan hubungi it',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('position'),
			);
		}
		return $data;

    }
}