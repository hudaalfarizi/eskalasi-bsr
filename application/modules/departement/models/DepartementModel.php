<?php

class DepartementModel extends CI_Model {
    function get_datatables(){
		$site_url = site_url();
		$this->datatables->select("
                    departement_name,
                    departement_type,
                    CONCAT(
						'<a data-uri=','''','$site_url','/departement/delete_departement','''',' data-source=','''',MD5(departement_id),'''',' class=','''','btn btn-sm btn-danger delete f-white','''','>delete</a>'
					) AS action,

		");
		$this->datatables->from("departement");
		return $this->datatables->generate();
	}
	function simpan($name,$tipe) {
		$insert = array(
			'departement_name' => $name,
			'departement_type' => $tipe,
		);
		$stmt = $this->db->insert('departement',$insert);
		if($stmt) {
			$data = array(
				'response' => 'success',
				'msg'      => 'berhasil simpan data',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('departement'),
			);
		} else {
			$data = array(
				'response' => 'failed',
				'msg'      => 'ada kesalahan saat simpan data silahkan hubungi it',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('departement'),
			);
		}
		return $data;
	}
	public function hapus() {
		$id = $this->input->post('id');
		$this->db->where('MD5(departement_id)',$id);
		$hapus = $this->db->delete('departement');
		if($hapus) {
			$data = array(
				'response' => 'success',
				'msg'      => 'berhasil hapus data',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('departement'),
			);
		} else {
			$data = array(
				'response' => 'failed',
				'msg'      => 'ada kesalahan saat hapus data silahkan hubungi it',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('departement'),
			);
		}
		return $data;
	}
}