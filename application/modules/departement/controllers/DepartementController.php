<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DepartementController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->permission->cek_permission('departement')) {
			$this->load->model('DepartementModel','model');
		} else {
			redirect('home');
		}		
	}
	public function index()
	{
		$data = array(
            'content' => 'departement',
        );
        $this->load->view('layouts/template',$data);
	}
	public function data_departement() {
		header('Content-Type: application/json');
		echo $this->model->get_datatables();
	}
	public function add_departement() {
		$this->form_validation->set_rules(
				'departement_name',
				'Nama Departement',
				'required',
				array('required'=>'Nama departement wajib di isi')
		);
		$this->form_validation->set_rules(
				'departement_type',
				'Nama Departement',
				'required',
				array('required'=>'Tipe departement wajib di isi')
		);
		if($this->form_validation->run() === FALSE) {
			$data = array(
				'response' => 'failed',
				'msg'      => validation_errors(),
				'csrfHash' => $this->security->get_csrf_hash(),
			);
		} else {
			$name = $this->input->post('departement_name');
			$tipe = $this->input->post('departement_type');
			$data = $this->model->simpan($name,$tipe);
		}
		echo json_encode($data);
	}
	public function delete_departement() {
		$data = $this->model->hapus();
		echo json_encode($data);
	}
}
