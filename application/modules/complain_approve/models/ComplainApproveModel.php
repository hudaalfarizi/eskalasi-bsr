<?php

class ComplainApproveModel extends CI_Model {
    function get_datatables(){
		$site_url = site_url();
		$this->datatables->select("
					ticket.ticket_code,
					departement.departement_name,
					problem_category.problem_category_name,
                    profile.name,
					problem_category.priority,
                    CONCAT(
						'<a href=','''','$site_url','/complain_approve/detail_complain?id=',MD5(ticket.ticket_id),'''','class=','''','btn btn-sm btn-success','''','>','lihat</a> '
					) AS action,

		");
		$this->datatables->from("ticket");
		$this->datatables->join("profile", "profile.user_id=ticket.user_id", "left");
		$this->datatables->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left");
		$this->datatables->join("departement", "departement.departement_id=ticket.departement_id_from", "left");
		if($this->session->departement_type == 'user') {
			$this->datatables->where("ticket.departement_id_from",$this->session->departement_id);
		} else {
			$this->datatables->where("ticket.departement_id_destination",$this->session->departement_id);
		}
		$this->datatables->where("ticket.status","menunggu_approve");
		return $this->datatables->generate();
	}
	function detail_complain($id){
		$data = $this->db->select("
								ticket.ticket_id,
								ticket.ticket_code,
								ticket.departement_id_destination,
								departement.departement_name,
								problem_category.problem_category_name,
								profile.name,
								ticket.description,
								ticket.created_at,
								ticket.updated_at,
								ticket.image,
								ticket.status,
								problem_category.priority,
						 ")
						 ->from("ticket")
						 ->join("profile", "profile.user_id=ticket.user_id", "left")
						 ->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left")
						 ->join("departement", "departement.departement_id=ticket.departement_id_from", "left")
						 ->where("md5(ticket.ticket_id)",$id)
						 ->get()->row();
		return $data;
	}
	function update_approve($ticket) {
		$cek_selesai = $this->db->get_where('ticket',['ticket_id'=>$ticket,'status'=>'menunggu_approve'])->num_rows();
		if($cek_selesai > 0) {
			$this->db->trans_start();
				$this->db->where("ticket_id",$ticket);
				$this->db->update("ticket",["status"=>"selesai"]);
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				$data = array(
					"response" => "failed",
					"msg"      => "complain gagal di selesaikan",
					'csrfHash' => $this->security->get_csrf_hash(),
					"url"      => site_url("complain_approve"),
				);
			} else {
				$data = array(
					"response" => "success",
					"msg"      => "anda berhasil selesaikan complain",
					'csrfHash' => $this->security->get_csrf_hash(),
					"url"      => site_url("complain_approve"),
				);
			}
		} else {
			$data = array(
				"response" => "failed",
				"msg"      => "complain sudah di selesaikan oleh yang lain",
				'csrfHash' => $this->security->get_csrf_hash(),
				"url"      => site_url("complain_approve"),
			);
		}
		return $data;
	}
}