<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CreateComplainController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if($this->permission->cek_permission('create_request_service')) {
            $this->load->model('CreateComplainModel','model');
        } else {
            redirect('home');
        }
    }
	public function index()
	{
        $departement = $this->model->get_departement();
		$data = array(
            'content'     => 'create_complain',
            'departement' => $departement,
        );
        $this->load->view('layouts/template',$data);
	}
    public function get_kategori() {
        $departement = $this->input->post('departement_id');
        $data        = $this->model->get_kategori($departement);
        echo json_encode($data);
    }
    public function do_upload() {
        $this->form_validation->set_rules(
                                'departement',
                                'Departement',
                                'required',
                                ['required'=>'Departement Harus Di Isi']
                            );
        $this->form_validation->set_rules(
                                'kategori_masalah',
                                'Kategori Masalah',
                                'required',
                                ['required'=>'Kategori Masalah Harus di isi']
                            );
        $this->form_validation->set_rules(
                                'detail_masalah',
                                'Detail Masalah',
                                'required',
                                ['required'=>'Detail Masalah Harus di isi']
                            );
        if($this->form_validation->run() === FALSE) {
            $data = array(
                'response' => 'failed',
                'msg'      => validation_errors(),
            );
        } else {
            if(!empty($_FILES['file']['name'])) {
                $config['upload_path']   ="./assets/images/complain_upload";
                $config['allowed_types'] ='gif|jpg|png|jpeg';
                $config['encrypt_name']  = TRUE;
                
                $this->load->library('upload',$config);
                if($this->upload->do_upload('file')){
                    $data = array('upload_data' => $this->upload->data());
                    $image  = $data['upload_data']['file_name'];                   
                    $data   = $this->model->simpan_upload($image);
                } else {
                    $data = array(
                        'response' => 'failed',
                        'msg'      => 'gagal simpan data, silahkan hubungi IT Support',
                        'csrfHash' => $this->security->get_csrf_hash(),
                    );
                }
            } else {
                $data = $this->model->simpan_upload();
            }
        }
        echo json_encode($data);
    }
}
