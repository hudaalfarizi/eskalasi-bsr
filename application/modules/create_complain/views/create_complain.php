<div class="col-12 col-sm-8 col-md-9">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Create Request Service</b></h5>
			<hr>
			<div class="information"></div>
			<br>
			<form class="form-file" data-uri="<?php echo site_url('co/buat_complain');?>" >
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ;?>" value="<?php echo $this->security->get_csrf_hash();?>" id="csrf">
				<table class="table">
					<tr>
						<td class="title-td">Departement</td>
						<td>:</td>
						<td>
							<select name="departement" id="departement" class="form-control">
								<option value=""></option>
								<?php foreach($departement as $row) {
									echo "<option value='".$row->departement_id."'> ". $row->departement_name ." </option>";
								} ;?>
							</select>
						</td>
                    </tr>
                    <tr>
						<td class="title-td">Kategori Masalah</td>
						<td>:</td>
						<td>
							<select name="kategori_masalah" id="kategori_masalah" class="form-control">
								<option value=""></option>
							</select>
						</td>
                    </tr>
                    <tr>
						<td class="title-td">Sisipkan Gambar</td>
						<td>:</td>
						<td>
							<input type="file" name="file" class="form_control" accept="image/x-png,image/gif,image/jpeg" />
						</td>
                    </tr>
                    <tr>
						<td class="title-td">Detail Masalah</td>
						<td>:</td>
						<td>
							<textarea name="detail_masalah" id="detail_masalah" rows="5" class="form-control"></textarea>
						</td>
                    </tr>
                </table>
                <input type="submit" name="submit" class="btn btn-success" value="simpan">
			</form>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
<script>
	$("#departement").on("change", function(){
		var departement_id = $(this).val();
		var csrfName       = $("#csrf").attr('name');
		var csrfHash       = $("#csrf").val();
		var myJson         = {departement_id:departement_id,[csrfName]:csrfHash};
		$.ajax({
			url      : "<?php echo site_url('get_kategori');?>",
			data     : myJson,
			type     : 'POST',
			dataType : 'JSON',

			success  : function(obj) {
				$("#kategori_masalah").empty();
				$.each(obj.data, function(i, val) {
					$("#kategori_masalah").append("<option value='" + val.problem_category_id + "'> " + val.problem_category_name + " </option>");
				});
				$("#csrf").val(obj.csrfHash);
			}
		})
	})
</script>
