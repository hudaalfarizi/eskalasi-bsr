<?php

class CreateComplainModel extends CI_Model {
    
    public function get_departement() {
        $query = $this->db->get_where('departement',['departement_type'=>'maintenance'])->result();
        return $query;
    }

    public function get_kategori($departement) {
    	$query = $this->db->get_where('problem_category',['departement_id'=>$departement])->result();
    	$data  = array(
    		'data'      => $query,
    		'csrfName'  => $this->security->get_csrf_token_name(),
    		'csrfHash'  => $this->security->get_csrf_hash(),
    	);
    	return $data;
	}
	
	public function simpan_upload($image = '') {
		$char        = 'CO';
		$waktu       = strtotime(date('Y-m-d H:i'));
		$ticket_code = $char . $waktu; 
		$departement      = $this->input->post('departement');
		$kategori_masalah = $this->input->post('kategori_masalah');
		$file_image       = $image;
		$detail_masalah   = $this->input->post('detail_masalah');
		$insert = array(
			'ticket_code'                => $ticket_code,
			'departement_id_destination' => $departement,
			'departement_id_from'        => $this->session->departement_id,
			'problem_category_id'        => $kategori_masalah,
			'image'						 => $image,
			'user_id'                    => $this->session->user_id,
			'description'                => $detail_masalah,
			'created_at'                 => date('Y-m-d H:i'),
			'status'			         => 'open',
		);
		$this->db->trans_start();
			$this->db->insert('ticket',$insert);
			$last_id = $this->db->insert_id();
			$tracelog = array(
				"ticket_id" => $last_id,
				"date"      => date('Y-m-d'),
				"open"      => date("H:i"),
			);
			$this->db->insert("tracelog_ticket",$tracelog);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$data = array(
				'response' => 'failed',
				'msg'	   => 'data gagal di simpan',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'	   => site_url('buat_complain'),
			);
		} else {
			$data = array(
				'response' => 'success',
				'msg'	   => 'data berhasil di simpan',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'	   => site_url('buat_complain'),
			);
		}
		return $data;
	}
}
