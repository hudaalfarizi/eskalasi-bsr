<?php defined('BASEPATH') or exit ('No direct Access Allowed');

class HomeController extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if($this->session->is_login === TRUE) {
            $this->load->model('HomeModel','model');
        } else {
            redirect('/');
        }
    }
    public function index() {
        $data = array(
            'content' => 'home',
        );
        $this->load->view('layouts/template',$data);
    }
}
