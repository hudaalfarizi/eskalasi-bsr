<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if($this->permission->cek_permission('user')) {
            $this->load->model('UserModel','model');
        } else {
            redirect('home');
        }
    }
	public function index()
	{
		$data = array(
            'content'     => 'data_user',
            'position'    => $this->model->get_position(),
        );
        $this->load->view('layouts/template',$data);
    }
    public function get_data_user() {
		header('Content-Type: application/json');
		echo $this->model->get_datatables();
    }
    public function save_user() {
        $this->form_validation->set_rules(
                'username',
                'Username',
                'required|is_unique[user.username]',
                    array(
                        'required'  =>'Username Harus Di Isi',
                        'is_unique' =>'Username sudah ada , silahkan ganti username lain'
                    )
        );
        $this->form_validation->set_rules(
                'idcard',
                'IdCard',
                'required|is_unique[user.idcard]',
                    array(
                        'required'  =>'id card Harus Di Isi',
                        'is_unique' =>'idcard sudah ada , silahkan ganti idcard lain'
                    )
        );
        $this->form_validation->set_rules(
                'position',
                'Position',
                'required',
                    array(
                        'required'  =>'Position Harus Di Isi',
                    )
        );
        if($this->form_validation->run() === FALSE) {
            $data = array(
                'response' => 'failed',
                'msg'      => validation_errors(),
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        } else {
            $username = $this->input->post('username');
            $idcard   = $this->input->post('idcard');
            $position = $this->input->post('position');
            $data = $this->model->save_user($username,$idcard,$position);
        }
        echo json_encode($data);
    }
    public function edit_user() {
        $id   = $this->input->post('id');
        $data = $this->model->edit_user($id);
        echo json_encode($data);
    }
    public function update_user() {
        $this->form_validation->set_rules(
            'username',
            'Username',
            'required',
                array(
                    'required'  =>'Username Harus Di Isi',
                    'is_unique' =>'Username sudah ada , silahkan ganti username lain'
                )
        );
        $this->form_validation->set_rules(
                'idcard',
                'IdCard',
                'required',
                    array(
                        'required'  =>'id card Harus Di Isi',
                        'is_unique' =>'idcard sudah ada , silahkan ganti idcard lain'
                    )
        );
        $this->form_validation->set_rules(
                'position',
                'Position',
                'required',
                    array(
                        'required'  =>'Position Harus Di Isi',
                    )
        );
        if($this->form_validation->run() === FALSE) {
            $data = array(
                'response' => 'failed',
                'msg'      => validation_errors(),
                'csrfHash' => $this->security->get_csrf_hash(),
            );
        } else {
            $user_id  = $this->input->post('id');
            $username = $this->input->post('username');
            $idcard   = $this->input->post('idcard');
            $position = $this->input->post('position');
            $data = $this->model->update_user($user_id,$username,$idcard,$position);
        }
        echo json_encode($data);
    }
    public function delete_user() {
		$data = $this->model->hapus();
		echo json_encode($data);
	}
}
