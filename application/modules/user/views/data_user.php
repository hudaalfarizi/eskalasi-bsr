<div class="col-12 col-sm-8 col-md-9">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title"><b>Data User</b></h5>
			<hr>	
			<br>
			<button class="btn btn-primary btn-sm" id="add">Add</button>
			<br><br>
			<div class="table-responsive">
				<table id="table-all" data-source="<?php echo site_url("get_data_user");?>">
					<thead>
						<th>Username</th>
						<th>Id Card</th>
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Position</th>
						<th>Departement</th>
						<th>Action</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="user_add">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add User</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="information"></div>
			<form class="form-post" data-uri="<?= site_url('save_user')?>">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf">
				<div class="modal-body">
					<label for="username">Username</label>
					<input type="text" name="username" class="form-control mb-1">
					<label for="idcard">IdCard</label>
					<input type="text" name="idcard" class="form-control mb-1">
					<label for="position">Position</label>
					<select name="position" id="position" class="form-control mb-1">
						<?php foreach($position as $row) { ?>
							<option value="<?= $row->position_id?>"><?= $row->position_name ?> - <?= $row->departement_name ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="submit">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="user_edit">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit User</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="information"></div>
			<form class="form-post" data-uri="<?= site_url('update_user')?>">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf">
				<div class="modal-body">
					<input type="hidden" id="user_id" name="id">
					<label for="username">Username</label>
					<input type="text" name="username" id="username" class="form-control mb-1">
					<label for="idcard">IdCard</label>
					<input type="text" name="idcard" id="idcard" class="form-control mb-1">
					<label for="position">Position</label>
					<select name="position" id="position" class="form-control mb-1">
						<option id="position_edit"></option>
						<?php foreach($position as $row) { ?>
							<option value="<?= $row->position_id?>"><?= $row->position_name ?> - <?= $row->departement_name ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="submit">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dt.css')?>">
<script src="<?php echo base_url('assets/js/dt.js');?>"></script>
<script>
$( document ).ready(function() {
    data_table();
});
function data_table()
{
	var url     = $('#table-all').attr('data-source');
	var token   = "<?php echo $this->security->get_csrf_hash(); ?>";
	var csrfname= "<?php echo $this->security->get_csrf_token_name(); ?>";

	$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
	{
		return {
			"iStart": oSettings._iDisplayStart,
			"iEnd": oSettings.fnDisplayEnd(),
			"iLength": oSettings._iDisplayLength,
			"iTotal": oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
			"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
		};
	};

	var table = $("#table-all").dataTable({
		initComplete: function() {
			var api = this.api();
			$('#mytable_filter input')
				.off('.DT')
				.on('input.DT', function() {
					api.search(this.value).draw();
			});
		},
			oLanguage: {
			sProcessing: "loading..."
		},
			processing: true,
			serverSide: true,
			ajax: {
				"url": url,
				"type": "POST",
				"data":{[csrfname]:token}
				},
				  columns: [
					  {"data": "username"},
					  {"data": "idcard"},
					  {"data": "name"},
					  {"data": "email"},
					  {"data": "phone"},
					  {"data": "position_name"},
					  {"data": "departement_name"},
					  {"data": "action"},
				],
			columnDefs: [{
					"targets" : [7],
					"orderable": false,
					"searchable": false,
				  }],
			order: [],
		rowCallback: function(row, data, iDisplayIndex) {
			var info = this.fnPagingInfo();
			var page = info.iPage;
			var length = info.iLength;
			$('td:eq(0)', row).html();
		}

	});
}

$("#add").on("click", function(){
	$("#user_add").modal('show');
})

$(document).on('click', '.edit_user', function (e) {
	var id  = $(this).attr('data-source');
	var url = $(this).attr('data-uri');
	var csrfName       = $("#csrf").attr('name');
	var csrfHash       = $("#csrf").val();
	$.ajax({
		url      : url,
		data     : {id:id,[csrfName]:csrfHash},
		type     : "POST",
		dataType : "JSON",

		success  : function(obj) {
			$("#user_edit").modal("show");
			$("#user_id").val(obj.user_id);
			$("#username").val(obj.username);
			$("#idcard").val(obj.idcard);
			$("#position_edit").text(obj.position_name + '-' + obj.departement_name);
			$("#position_edit").val(obj.position_id);
			$(".csrf").val(obj.csrfHash);
		}
	})
	
});
</script>
