<?php

class UserModel extends CI_Model {
    function get_datatables(){
		$site_url = site_url();
        $this->datatables->select("
                    u.username,
					u.idcard,
					pr.name,
					pr.email,
					pr.phone,
					p.position_name,
                    d.departement_name,
					CONCAT(
						'<a data-uri=','''','$site_url','/user/edit_user','''',' data-source=','''',MD5(u.user_id),'''',' class=','''','btn btn-sm btn-warning edit_user','''','>edit</a> ',
						'<a data-uri=','''','$site_url','/user/delete_user','''',' data-source=','''',MD5(u.user_id),'''',' class=','''','btn btn-sm btn-danger f-white delete','''','>delete</a> '
					) AS action,

		");
		$this->datatables->from("user u");
		$this->datatables->join("profile pr", "pr.user_id=u.user_id", "left");
		$this->datatables->join("position p", "p.position_id=u.position_id", "left");
		$this->datatables->join("departement d", "d.departement_id=p.departement_id", "left");
		if($this->session->departement_type == 'user' || $this->session->departement_type == 'maintenance') {
			$this->datatables->where("d.departement_id",$this->session->departement_id);
		}
		return $this->datatables->generate();
	}
	function get_position() {
		$query = $this->db->select('
								p.position_id,
								p.position_name,
								d.departement_name,
							')
						   ->from('position p')
						   ->join('departement d','d.departement_id=p.departement_id','left')
						   ->get()->result();
		return $query;
	}
	function save_user($username,$idcard,$position) {
		$duser = array(
			'idcard'      => $idcard,
			'username'    => $username,
			'password'    => sha1('123456'),
			'position_id' => $position,
		);
		$this->db->trans_start();
			$this->db->insert("user",$duser);
			$user_id = $this->db->get_where('user',['username'=>$username]);
			$this->db->insert('profile',['user_id'=>$user_id->row()->user_id]);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
				$data = array(
					"response" => "failed",
					"msg"      => "user gagal di tambahkan",
					'csrfHash' => $this->security->get_csrf_hash(),
					"url"      => site_url("user"),
				);
		} else {
			$data = array(
				"response" => "success",
				"msg"      => "anda berhasil tambahkan user",
				'csrfHash' => $this->security->get_csrf_hash(),
				"url"      => site_url("user"),
			);
		}
		return $data;
	}
	function edit_user($id) {
		$query = $this->db->select('
								u.username,
								u.idcard,
								u.user_id,
								p.position_id,
								p.position_name,
								d.departement_name
							')
						  ->from('user u')
						  ->join('position p','p.position_id=u.position_id','left')
						  ->join('departement d','d.departement_id=p.departement_id','left')
						  ->where('MD5(user_id)',$id)
						  ->get()->row();
		return $query;
	}
	function update_user($user_id,$username,$idcard,$position) {
		$cek = $this->db->get_where('user',['user_id'=>$user_id]);
		if($cek->row()->username != $username) {
			$this->form_validation->set_rules(
					'username',
					'Username',
					'required|is_unique[user.username]',
						array(
							'required'  =>'Username Harus Di Isi',
							'is_unique' =>'Username sudah ada , silahkan ganti username lain'
						)
			);
		} elseif($cek->row()->idcard != $idcard) {
			$this->form_validation->set_rules(
					'idcard',
					'IdCard',
					'required|is_unique[user.idcard]',
						array(
							'required'  =>'id card Harus Di Isi',
							'is_unique' =>'idcard sudah ada , silahkan ganti idcard lain'
						)
			);
		}
		if($this->form_validation->run() === FALSE) {
			$data = array(
                'response' => 'failed',
                'msg'      => validation_errors(),
                'csrfHash' => $this->security->get_csrf_hash(),
            );
		} else {
			$duser = array(
				'idcard'      => $idcard,
				'username'    => $username,
				'position_id' => $position,
			);
			$this->db->where('user_id',$user_id);
			$update = $this->db->update('user',$duser);
			if($update) {
				$data = array(
					"response" => "success",
					"msg"      => "berhasil update data",
					'csrfHash' => $this->security->get_csrf_hash(),
					"url"      => site_url("user"),
				);
			} else {
				$data = array(
					"response" => "failed",
					"msg"      => "ada kesalahan saat update data",
					'csrfHash' => $this->security->get_csrf_hash(),
					"url"      => site_url("user"),
				);
			}
		}
		return $data;
	}
	public function hapus() {
		$id = $this->input->post('id');
		$this->db->trans_start();
			$this->db->where('MD5(user_id)',$id);
			$this->db->delete('user');
			$this->db->where('MD5(user_id)',$id);
			$this->db->delete('profile');
		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE) {
			$data = array(
				'response' => 'failed',
				'msg'      => 'ada kesalahan saat hapus data silahkan hubungi it',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('user'),
			);
			
		} else {
			$data = array(
				'response' => 'success',
				'msg'      => 'berhasil hapus data',
				'csrfHash' => $this->security->get_csrf_hash(),
				'url'      => site_url('user'),
			);
		}
		return $data;
    }
}
