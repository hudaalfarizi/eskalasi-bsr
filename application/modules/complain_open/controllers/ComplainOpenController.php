<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ComplainOpenController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->permission->cek_permission('service_request_open')) {
			$this->load->model('ComplainOpenModel','model');
		} else {
			redirect('home');
		}
		
	}
	public function index()
	{
		$data = array(
            'content' => 'data_complain',
        );
        $this->load->view('layouts/template',$data);
	}
	public function data_complain_open() {
		header('Content-Type: application/json');
		echo $this->model->get_datatables();
	}
	public function detail_complain() {
		$id     = $_GET['id'];
		$result = $this->model->detail_complain($id);
		$data   = array(
			'content' => 'detail_complain',
			'result'  => $result,
		);
		$this->load->view('layouts/template',$data);
	}
}
