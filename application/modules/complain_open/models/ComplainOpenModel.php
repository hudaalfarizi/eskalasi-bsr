<?php

class ComplainOpenModel extends CI_Model {
    function get_datatables(){
		$site_url = site_url();
		$this->datatables->select("
					ticket.ticket_code,
					departement.departement_name,
					problem_category.problem_category_name,
                    profile.name,
                    problem_category.priority,
					CONCAT(
						'<a href=','''','$site_url','/complain_open/detail_complain?id=',MD5(ticket.ticket_id),'''','class=','''','btn btn-sm btn-success','''','>','lihat</a> '
					) AS action,
		");
		$this->datatables->from("ticket");
		$this->datatables->join("profile", "profile.user_id=ticket.user_id", "left");
		$this->datatables->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left");
		$this->datatables->join("departement", "departement.departement_id=ticket.departement_id_from", "left");
		$this->datatables->where("ticket.departement_id_from",$this->session->departement_id);
		$this->datatables->where("ticket.status","open");
		return $this->datatables->generate();
	}
	function detail_complain($id){
		$data = $this->db->select("
								ticket.ticket_id,
								ticket.ticket_code,
								departement.departement_name,
								problem_category.problem_category_name,
								profile.name,
								ticket.description,
								ticket.created_at,
								ticket.updated_at,
								ticket.image,
								problem_category.priority,
						 ")
						 ->from("ticket")
						 ->join("profile", "profile.user_id=ticket.user_id", "left")
						 ->join("problem_category", "problem_category.problem_category_id=ticket.problem_category_id", "left")
						 ->join("departement", "departement.departement_id=ticket.departement_id_from", "left")
						 ->where("md5(ticket.ticket_id)",$id)
						 ->get()->row();
		return $data;
	}
}