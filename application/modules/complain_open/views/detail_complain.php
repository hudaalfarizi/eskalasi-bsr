<div class="col-12 col-sm-8 col-md-9">
	<div class="card">
		<div class="card-body">
            <h5><b>Detail Complain</b></h5>
            <hr>
            <br>
            <table class="table table-striped table-hover">
                <tr>
                    <td class="title-td">kode Tiket</td>
                    <td>:</td>
                    <td><?php echo htmlspecialchars($result->ticket_code) ;?></td>
                </tr>
                <tr>
                    <td class="title-td">Departement</td>
                    <td>:</td>
                    <td><?php echo htmlspecialchars($result->departement_name) ;?></td>
                </tr>
                <tr>
                    <td class="title-td">Kategori Masalah</td>
                    <td>:</td>
                    <td><?php echo htmlspecialchars($result->problem_category_name) ;?></td>
                </tr>
                <tr>
                    <td class="title-td">Nama Pelapor</td>
                    <td>:</td>
                    <td><?php echo htmlspecialchars($result->name) ;?></td>
                </tr>
                <tr>
                    <td class="title-td">Detail Masalah</td>
                    <td>:</td>
                    <td><?php echo htmlspecialchars($result->description) ;?></td>
                </tr>
                <tr>
                    <td class="title-td">Waktu Komplain</td>
                    <td>:</td>
                    <td><?php echo htmlspecialchars($result->created_at) ;?></td>
                </tr>
                <?php if($result->image != '') { ?>
                    <tr>
                        <td class="title-td">Gambar</td>
                        <td>:</td>
                        <td><a target="_blank" href="<?php echo base_url('assets/images/complain_upload/'.$result->image);?>">images</a></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td class="title-td">Prioritas Masalah</td>
                    <td>:</td>
                    <td><?php echo htmlspecialchars($result->priority) ;?></td>
                </tr>
            </table>  
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/general.js');?>"></script>
