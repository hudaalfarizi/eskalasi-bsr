-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `activity_assignment`;
CREATE TABLE `activity_assignment` (
  `activity_assignment_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `root_cause` text NOT NULL,
  `handling` text NOT NULL,
  `complied_by` int(11) NOT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `image` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`activity_assignment_id`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `departement`;
CREATE TABLE `departement` (
  `departement_id` int(11) NOT NULL AUTO_INCREMENT,
  `departement_name` varchar(100) NOT NULL,
  `departement_type` varchar(100) NOT NULL,
  PRIMARY KEY (`departement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `departement` (`departement_id`, `departement_name`, `departement_type`) VALUES
(1,	'maintenance',	'maintenance'),
(2,	'finance',	'user'),
(3,	'superadmin',	'superadmin'),
(4,	'acl',	'maintenance');

DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `module` (`module_id`, `module_name`) VALUES
(1,	'create_request_service'),
(2,	'incoming_service_request'),
(3,	'service_request_open'),
(4,	'departement'),
(5,	'position'),
(6,	'permission'),
(7,	'service_request_in_process'),
(8,	'service_request_completed'),
(10,	'done'),
(11,	'service_approval'),
(12,	'all_request_service'),
(13,	'user'),
(14,	'pending');

DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `position_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `permission` (`permission_id`, `module_id`, `position_id`, `user_id`) VALUES
(1,	1,	2,	NULL),
(2,	2,	1,	NULL),
(3,	3,	2,	NULL),
(4,	4,	4,	NULL),
(5,	5,	4,	NULL),
(6,	6,	4,	NULL),
(8,	2,	2,	NULL),
(9,	7,	2,	NULL),
(10,	8,	2,	NULL),
(11,	3,	1,	NULL),
(12,	7,	1,	NULL),
(13,	8,	1,	NULL),
(14,	10,	1,	NULL),
(15,	2,	5,	NULL),
(16,	3,	5,	NULL),
(17,	7,	5,	NULL),
(18,	8,	5,	NULL),
(19,	11,	5,	NULL),
(20,	11,	NULL,	1),
(21,	12,	2,	NULL),
(22,	12,	1,	NULL),
(23,	13,	4,	NULL),
(24,	1,	1,	NULL),
(25,	14,	1,	NULL),
(26,	1,	6,	NULL),
(27,	3,	6,	NULL),
(28,	2,	6,	NULL),
(30,	7,	6,	NULL),
(31,	8,	6,	NULL),
(32,	10,	6,	NULL),
(33,	14,	6,	NULL),
(34,	11,	1,	NULL);

DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `position_id` int(11) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(100) NOT NULL,
  `departement_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `position` (`position_id`, `position_name`, `departement_id`) VALUES
(1,	'clerk',	1),
(2,	'clerk',	2),
(4,	'superadmin',	3),
(5,	'supervisor',	1),
(6,	'clerk',	4);

DROP TABLE IF EXISTS `problem_category`;
CREATE TABLE `problem_category` (
  `problem_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `departement_id` int(11) NOT NULL,
  `problem_category_name` varchar(100) NOT NULL,
  `priority` varchar(100) NOT NULL,
  PRIMARY KEY (`problem_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `problem_category` (`problem_category_id`, `departement_id`, `problem_category_name`, `priority`) VALUES
(1,	1,	'perlengkapan',	'hight');

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `profile_picture` varchar(100) NOT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `profile` (`profile_id`, `user_id`, `name`, `email`, `phone`, `profile_picture`) VALUES
(1,	1,	'Jaynuri',	'jaynuri@gmail.com',	'081911566664',	'566ee81fdca50e89d5675cbf228659e2.png'),
(2,	2,	'hudapos',	'hudaalfarizi21@gmail.com',	'081911566664',	'71e1935eb062313ec26aa6ae4d3a969e.jpeg'),
(3,	3,	'Mutiara Camila',	'mutiara@gmail.com',	'081988928918',	''),
(4,	4,	'bella',	'bella@gmail.com',	'09189128281',	''),
(5,	5,	'',	'',	'',	''),
(6,	6,	'',	'',	'',	'');

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_code` varchar(100) NOT NULL,
  `departement_id_destination` int(11) NOT NULL,
  `departement_id_from` int(11) NOT NULL,
  `problem_category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `teknisi` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tracelog_ticket`;
CREATE TABLE `tracelog_ticket` (
  `tracelog_ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `pic` int(11) NOT NULL,
  `date` date NOT NULL,
  `open` time NOT NULL,
  `approve` time NOT NULL,
  `execute` time NOT NULL,
  `pending` time NOT NULL,
  `finish` time NOT NULL,
  `manhours` varchar(100) NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`tracelog_ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `idcard` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` tinytext NOT NULL,
  `position_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`user_id`, `idcard`, `username`, `password`, `position_id`) VALUES
(1,	'2015015915',	'jaynuri',	'7c222fb2927d828af22f592134e8932480637c0d',	1),
(2,	'2015015916',	'huda',	'7c222fb2927d828af22f592134e8932480637c0d',	2),
(3,	'2019898787',	'mutiara',	'7c222fb2927d828af22f592134e8932480637c0d',	4),
(4,	'2090',	'bella',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
(5,	'201698878',	'ipul',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	1),
(6,	'2016781781',	'aceng',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	2);

-- 2021-01-22 02:08:11
