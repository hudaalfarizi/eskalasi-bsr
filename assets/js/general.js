$('.form-post').submit(function (e) {
	e.preventDefault();

	$('.information').css('display', 'none');

	$.ajax({
		url: $(this).data('uri'),
		type: 'POST',
		dataType: 'JSON',
		data: $(this).serialize(),

		success: function (data) {
			if (data.response == 'success') {
				Swal.fire({
					icon: 'success',
					title: 'Selamat !',
					text: data.msg,
					type: "success"
				}).then(function () {
					window.location = data.url;
				});
			} else if (data.response == 'failed') {

				$('.information').css('display', 'block');
				$('.information').addClass('alert alert-danger');
				$('.information').html(data.msg);
				Swal.fire({
					icon: 'error',
					title: 'Ada Kesalahan !',
					text: 'terjadi kesalahan',
					type: "error"
				});
				$("#csrf").val(data.csrfHash);
			}
		}
	});
});

$('.form-file').submit(function (e) {
	e.preventDefault();
	$('.information').css('display', 'none');

	$.ajax({
		url: $(this).data('uri'),
		type: 'POST',
		dataType: 'JSON',
		data: new FormData(this),
		processData: false,
		contentType: false,
		cache: false,
		async: false,

		success: function (data) {
			if (data.response == 'success') {
				Swal.fire({
					icon: 'success',
					title: 'Selamat !',
					text: data.msg,
					type: "success"
				}).then(function () {
					window.location = data.url;
				});
			} else if (data.response == 'failed') {

				$('.information').css('display', 'block');
				$('.information').addClass('alert alert-danger');
				$('.information').html(data.msg);
				Swal.fire({
					icon: 'error',
					title: 'Ada Kesalahan !',
					text: 'terjadi kesalahan',
					type: "error"
				});
				$("#csrf").val(data.csrfHash);
			}
		}
	});

})
$("#file").on("change", function () {
	$(".loading").text('loading.........');

	var csrfName = $("#csrf1").attr('name');
	var csrfHash = $("#csrf1").val();
	var user_id = $("#user_id").val();
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('user_id', user_id);
	form_data.append(csrfName, csrfHash);
	$.ajax({
		url: $(this).data('uri'),
		type: 'POST',
		dataType: 'JSON',
		data: form_data,
		processData: false,
		contentType: false,
		cache: false,
		async: false,

		success: function (obj) {
			location.reload();
		}
	});
});
$(document).on('click', '.delete', function (e) {
	var id  = $(this).attr('data-source');
	var url = $(this).attr('data-uri');
	var csrfName       = $("#csrf").attr('name');
	var csrfHash       = $("#csrf").val();
	Swal.fire({
		title: 'Hapus Data',
		text: "apakah anda yakin?",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya!'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'JSON',
				data: {
					id: id,
					[csrfName]:csrfHash,
				},

				success: function (data) {
					if (data.response == 'success') {
						Swal.fire({
							icon: 'success',
							title: 'Selamat !',
							text: data.msg,
							type: "success"
						}).then(function () {
							window.location = data.url;
						});
					} else if (data.response == 'failed') {

						$('.information').css('display', 'block');
						$('.information').addClass('alert alert-danger');
						$('.information').html(data.msg);
						$("#csrf").val(data.csrfHash);
						Swal.fire({
							icon: 'error',
							title: 'Ada Kesalahan !',
							text: 'terjadi kesalahan',
							type: "error"
						});
					}
				}
			})
		}
	})

});
$(document).on('click', '.edit', function (e) {
	var id  = $(this).attr('data-source');
	var url = $(this).attr('data-uri');
	var csrfName       = $("#csrf").attr('name');
	var csrfHash       = $("#csrf").val();
	$(".modal").modal("show");
	$.ajax({
		url      : url,
		data     : {id:id,[csrfName]:csrfHash},
		type     : "POST",
		dataType : "JSON",

		success  : function(obj) {
			$(".modal").modal("show");
			$("#position_id").val(obj.msg.position_id);
			$("#position_name").val(obj.msg.position_name);
			$("#departement_edit").text(obj.msg.departement_name);
			$("#departement_edit").val(obj.msg.departement_id);
			$(".csrf").val(obj.csrfHash);
		}
	})
	
});
